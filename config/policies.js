/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {
 // '*': true,
  //'*': 'is-logged-in',
  '*': ['tokenAuth', 'access-action-crud', 'access-action-detail','Logs'],
  // Bypass the `is-logged-in` policy for:
  'entrance/*': true,
  'user/token': true,
  'user/login-with-google': true,
  'account/logout': true,
  'user/req-change-password': true,
  'user/change-password-byotp': true,
  'view-homepage-or-redirect': true,
  'deliver-contact-form-message': true,

};
