module.exports = function tokenAuth(req, res, next) {
//	console.log("user",req.user);
	if(req.method != 'GET'){
		var data = {
			object : req.options.controller,
			action : req.options.action,
			method : req.method,
			params : JSON.stringify(req.allParams()),
			user : req.user.id,
			fullname : req.user.username
		}
		Logs.create(data).exec(function(err,createDone){
			if(err) console.log(err);
		});
	}
	next();
}
