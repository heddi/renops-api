module.exports = async function (req, res, proceed) {

  // First, check whether the request comes from a logged-in user.
  // > For more about where `req.me` comes from, check out this app's
  // > custom hook (`api/hooks/custom/index.js`).
  req.user = req.token.user;
  //console.log('req.options.controller', req.options.action);
  var row = await sails.helpers.checkPermissions(req.token.user.id, req.token.user.userGroup).intercept('orgNotFound', 'notFound');
  var rights = JSON.parse(row.apiActionDisabled);
  var action = req.options.action;

  //sails.log('rights____', rights);
  //sails.log('exist action', typeof rights['default/test-accessx']);
  //sails.log('this.req.method', req.method);
  //if (!_.contains(rights, 'GET')) {
  if (typeof rights[action] !== 'undefinded' && rights[action]) {
    throw 'unauthorized';
  }
  // IWMIH, we've got ourselves a "super admin".
  return proceed();

};
