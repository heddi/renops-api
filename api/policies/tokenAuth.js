module.exports = function tokenAuth(req, res, next) {
    var token;

    if (req.headers && req.headers.authorization) {
        var parts = req.headers.authorization.split(' ');

        if (parts.length == 2) {
            var scheme = parts[0],
                credentials = parts[1];

            if (/^Bearer$/i.test(scheme)) {
                token = credentials;
            }
        } else {
            return res.unauthorized('Format is Authorization: Bearer [token]');
        }
    } else if (req.param('token')) {
        token = req.param('token');
        // Delete the token from param to not mess with blueprints
        if(req.query.token) delete req.query.token;
        //console.log('req.body',req.body);
        if(req.body != undefined) delete req.body.token;
    } else {
        return res.unauthorized('No Authorization header (token) was found');
    }

    TokenManager.verifyToken(token, function(err, token) {
        if (err){
        	console.log('policy - tokenAuth - AuthManager.verifyToken - err: ', err);
        	//The token has expired
        	if(err.name == 'TokenExpiredError') return res.unauthorized('TokenExpiredError');
        	return res.unauthorized('The token is not valid');
        }
        req.token = token;
        //buat kebutuhan module auth-permission
        //console.log('tokenAuth - req.token.user: ',req.token.user)
        req.user = req.token.user;
        sails.log('tokenAuth - req.token: ', req.token);
        next();
    });
};
