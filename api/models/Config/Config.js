module.exports = {
    tableName: 'config',
    migrate: 'safe',
    attributes: {
        groupConfig: 'string',
        variable: 'string',
        valueConfig: 'string',
        valueType: 'string',
    },
    
};