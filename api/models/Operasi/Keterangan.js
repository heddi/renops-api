module.exports = {
    tableName: 'md_keterangan_operasi',
    schema: true,
    migrate: 'safe',
    attributes: {
      keterangan: {
        type: 'string',
        allowNull: true
    },
    },

}
