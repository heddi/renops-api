module.exports = {
  tableName: 'md_target_operasi_item',
  schema: true,
  migrate: 'safe',
  attributes: {
    target_operasi_id: {
      model: 'Target'
    },
    kelompok: 'string',
    sub_kelompok: 'string',
    uraian: 'string',
    keterangan_operasi_id: {
      model: 'Keterangan'
    },
    rencana_operasi_id: {
      model: 'Keterangan'
    },
  },

}
