module.exports = {
    tableName: 'ref_jenis_operasi',
    schema: true,
    migrate: 'safe',
    attributes: {
      jenis_operasi: {
        type: 'string',
        allowNull: true
    },
    },

}
