module.exports = {
  tableName: 'md_rencana_operasi',
  schema: true,
  migrate: 'safe',
  attributes: {
    tahun: {
        type: 'string',
        allowNull: true
    },
    jenis_operasi_id: {
      model: 'Jenis'
    },
    no_operasi: 'string',
    tgl_mulai :{
      type: 'string', columnType: 'datetime'
    },
    tgl_akhir :{
      type: 'string', columnType: 'datetime'
    },
    total_pelaksanaan:'number',
    catatan:{
      type:'string',
      allowNull: true
    },
    file_sprint:{
      type:'string',
      allowNull: true
    },
    status_renops:'string',
    status_operasi:'string'
  },

}
