module.exports = {
  tableName: 'md_katalog_target_operasi',
  schema: true,
  migrate: 'safe',
  attributes: {
    target_operasi_id: {
      model: 'Target'
    },
    kelompok: 'string',
    sub_kelompok: 'string',
    uraian: 'string',
    keterangan_operasi_id: {
      model: 'Keterangan'
    }
  },

}
