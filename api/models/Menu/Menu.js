module.exports = {
    tableName: 'menu',
    migrate: 'safe',
    attributes: {
        title: 'string',
        active: 'number',
        parent: 'number',
        path: {
            type: 'string'
        },
        icon: {
        	type:'string',
        	allowNull: true
        },
        alias: 'string',
        sorting: 'number',
        className: {
        	type:'string',
        	allowNull: true
        },
        badge: {
        	type:'string',
        	allowNull: true
        },
        badgeClass: {
        	type:'string',
        	allowNull: true
        },
        isExternalLink: {
        	type:'string',
        	allowNull: true
        },
        isNavHeader: {
        	type:'string',
        	allowNull: true
        },
    }
};