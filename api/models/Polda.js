module.exports = {
  tableName: 'md_polda',
  schema: true,
  migrate: 'safe',
  attributes: {
    nama_polda: {
        type: 'string',
        allowNull: true
    },
    wilayah_hukum: {
      type: 'string',
      allowNull: true
    },
  },

}
