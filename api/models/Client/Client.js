module.exports = {
    tableName: 'clients',
    schema: true,
    migrate: 'safe',
    attributes: {
        'name': 'string',
        'email': 'string',
        'phone': 'string',
        'address': 'string',
        'password': 'string',
        'status': 'number',
        'point': 'number',
        email_verified: 'number',
        email_verified_date: {
            type: 'string',
            columnType: 'datetime'
        },
        link_verified: 'string',
        otp: 'string',
        otp_time: {
            type: 'string',
            columnType: 'datetime'
        },
        'otp_valid_date': {
            type: 'string',
            columnType: 'datetime'
        },
        'approved_date': {
            type: 'string',
            columnType: 'datetime'
        },
        'customer_no': 'string',
        plateno: 'string',
        username: 'string',
        os: 'string',
        gcm_id: 'string',
        last_login: {
            type: 'string',
            columnType: 'datetime'
        }
    },

}