module.exports = {
    tableName: 'user_group',
    attributes: {
        groupName: {
            type: 'string',
        },
        apiCrud: {
            type: 'string',
            allowNull: true
        },
        apiActionDisabled: {
            type: 'string',
            allowNull: true
        },
        createdBy: {
            model: 'User'
        },
        updatedBy: {
            model: 'User'
        },
    },
};

