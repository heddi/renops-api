module.exports = {
    tableName: 'authorization',
    attributes: {
        idObject: {
            type: 'number'
        },
        menu: {
        	columnName:'idMenu',
            type: 'number'
        },
        bitCreate: {
            type: 'number'
        },
        bitEdit: {
            type: 'number'
        },
        bitDelete: {
            type: 'number'
        },
        bitView: {
            type: 'number'
        },
        bitMenu: {
            type: 'number'
        },
        bitCancel: {
            type: 'number'
        },
        bitApprove: {
            type: 'number'
        },
        bitExcel: {
            type: 'number'
        },
    },
};

