module.exports = {
    tableName: 'logs',
    attributes: {
        object: {
            type: 'string',
        },
        action: {
            type: 'string',
            allowNull: true
        },
        method: {
            type: 'string',
            allowNull: true
        },
        params: {
            type: 'string',
            allowNull: true
        },
        user: {
           columnName:'idUser',
            model:'User'
        },
        fullname: {
            type: 'string',
            allowNull: true
        },
    },
};

