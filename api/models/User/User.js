module.exports = {
    tableName: 'user',
    migrate: 'safe',
    attributes: {
        fullname: 'string',
        email: 'string',
        username: 'string',
        password: 'string',
        otp: {
          type: 'string',
          allowNull: true
       },
        userGroup: {
        	columnName:'idUserGroup',
            model: 'UserGroup'
        },
        active: {
            type: 'number'
        },
        lastlogin: {
            type: 'ref',
            columnType: 'datetime'
        },
        createdBy: {
            model: 'User'
        },
        updatedBy: {
            model: 'User'
        }
    },
    customToJSON: function () {
        return _.omit(this, ['password']);
    }
};
