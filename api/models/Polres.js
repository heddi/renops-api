module.exports = {
  tableName: 'md_polres',
  schema: true,
  migrate: 'safe',
  attributes: {
    nama_polres: {
        type: 'string',
        allowNull: true
    },
    polda_id: {
      model: 'Polda'
    },
  },

}
