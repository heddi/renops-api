var moment = require('moment');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var secret = 'aEfd09qu43f09qfj94qf*&H##T';

module.exports = {
    issueToken: function(payload) {

        if(payload.exp){ //jika diset secara manual
        	var token = jwt.sign(payload, secret);
        }else{ //gunakan default 1x24 jam

        	var token = jwt.sign(payload, secret, { expiresIn: '1d' });
        }

        return token;
    },

    verifyToken: function(token, cb){
        return jwt.verify(token, secret, {}, cb);
    },

    decode: function(token){
    	return jwt.decode(token, {complete: true});
    },

    getReqToken: function(req, done){
    	var tokenReqParamName = 'token';

	    if (req.headers && req.headers.authorization) {
	        var parts = req.headers.authorization.split(' ');

	        if (parts.length == 2) {
	            var scheme = parts[0],
	                credentials = parts[1];

	            if (/^Bearer$/i.test(scheme)) {
	                return done(null, credentials);
	            }
	        } else {
	            return done('Format is Authorization: Bearer [token]');
	        }
	    } else if (req.param(tokenReqParamName)) {
	    	var token = req.param(tokenReqParamName);

	    	//delete biar ga bentrok sama criteria
	    	if(req.query && req.query[tokenReqParamName]) delete req.query[tokenReqParamName];
	    	if(req.body && req.body[tokenReqParamName]) delete req.body[tokenReqParamName];

	    	console.log('getReqToken - token: ', token);

	        return done(null, token);
	    } else {
	        return done('No Authorization header (' + tokenReqParamName + ') was found');
	    }
    },
    issueRefreshToken: function(){
		crypto.randomBytes(256, function (ex, buffer) {
	    	if (ex) return ex;
	    	var token = crypto.createHash('sha1').update(buffer).digest('hex');
	    	return token;
	  	});
    }
};
