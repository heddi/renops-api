var _ = require('lodash');
var admin = require('firebase-admin')
var cwd = process.cwd();

module.exports = {
	addZero: function (str, max) {
		str = str.toString();
		return (str.length < max ? this.addZero("0" + str, max) : str);
	},
	sendMail: function (options, cb) {
		const nodemailer = require('nodemailer');
		Config.find({ groupConfig: 'SMTP_MAIL' }).exec(function (err, foundConfig) {
			if(err) console.log(err);
			var hashMap = {};
			for (var i = 0; i < foundConfig.length; i++) {
				hashMap[foundConfig[i].variable] = foundConfig[i].valueConfig;
			}
			//console.log('hashMap',hashMap);
			// create reusable transporter object using the default SMTP transport
			var transporter = nodemailer.createTransport({
				host: hashMap['host'],
				port: hashMap['port'],
				secure: true, // secure:true for port 465, secure:false for port 587
				auth: {
					user: hashMap['user'],
					pass: hashMap['pass']
				}
			});

			// setup email data with unicode symbols
			var mailOptions = {
				from: (options.from) ? options.from : hashMap['user'],  // sender address
				to: options.to, // list of receivers
				subject: options.subjectMail, // Subject line
				text: options.textBodyMail, // plain text body
				html: options.htmlBodyMail // html body
			};
			console.log('mailOptions', mailOptions);
			// send mail with defined transport object
			try {
				transporter.sendMail(mailOptions, (error, info) => {
					if (error) {
						cb(error);
						console.log(error);
					} else (
						cb(null, 'message id : ' + info.messageId + ',info :' + info.response)
					)
					//console.log('Message %s sent: %s', info.messageId, info.response);
				});
			} catch (err) {
				cb(err.message);
			}

		});

	}
}
