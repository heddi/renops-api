module.exports = {
    treemenu: function (idObject, showinmenu, next) {
        var start = new Date();
        var async = require("async");
        var whereShowMenu = ' '
		if(showinmenu) whereShowMenu = ' AND  b.id is not null ';
		
        async.waterfall([
            function getData(cb) {
                if(idObject){
                	var sql = `SELECT 
							   a.*,
								b.id idAuth,
								b.idMenu,
								b.bitCreate,
								b.bitEdit,
								b.bitDelete,
								b.bitView,
								b.bitMenu,
								b.bitCancel,
								b.bitApprove,
								b.bitExcel,
							    IF(b.id IS NOT NULL, 1,0) permission
							FROM
							    menu a
							        LEFT JOIN
							    authorization b ON a.id = b.idMenu    AND idObject= ` + idObject + ` 
							WHERE
							    a.active = 1 AND a.deleted=0 `+ whereShowMenu +`
							ORDER BY sorting`;
               } else {
               		//Query ini untuk menampilkan tree menu di menu manager
               		var sql = `SELECT  * FROM menu WHERE deleted = 0 ORDER BY sorting`;
               }
                
               

                //console.log('sql',sql);

                User.getDatastore().sendNativeQuery(sql, function (err, rawResult) {
                    if (err) return cb(err, null);
                    if (rawResult) {
                        cb(null, rawResult.rows);
                    }
                });
            },
            function listToTree(rawResult, cb) {

                var data = rawResult
                var ID_KEY = 'id';
                var PARENT_KEY = 'parent';
                var CHILDREN_KEY = 'submenu';

                var item, id, parentid;
                var map = {};

                async.waterfall([
                    function mapping(cb2) {
                        for (var i = 0; i < data.length; i++) { // make cache
                            if (data[i][ID_KEY]) {
                                map[data[i][ID_KEY]] = data[i];
                                data[i][CHILDREN_KEY] = [];
                            }
                        }
                        cb2(null, data);
                    },
                    function treeMenu(data, cb2) {

                        for (var i = 0; i < data.length; i++) {
                            if (data[i][PARENT_KEY]) { // is a child
                                if (map[data[i][PARENT_KEY]]) // for dirty data
                                {
                                    map[data[i][PARENT_KEY]][CHILDREN_KEY].push(data[i]); // add child to parent
                                    data.splice(i, 1); // remove from root
                                    i--; // iterator correction
                                } else {
                                    data[i][PARENT_KEY] = 0; // clean dirty data
                                }
                            }
                        };
                        cb2(null, data);



                    }
                ], function allDone(err, data) {
                    cb(null, data);
                });
                //return data;
            }
        ], function allDone(err, source) {
            console.log('err',err);
            var time = new Date() - start;
            sails.log.warn('time==>', time);
            next(source);
        });
        //console.log('sql',sql);
    } //end treeMenu
}
