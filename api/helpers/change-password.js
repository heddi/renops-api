module.exports = {
    friendlyName: 'Check permissions',
    description: 'Look up a user\'s "rights" within a particular organization.',
    inputs: {
        userId: { type: 'number', required: true },
        orgId: { type: 'number', required: true }
    },
    exits: {
        success: {
            outputFriendlyName: 'Rights',
            outputDescription: `A user's "rights" within an org.`,
            outputType: ['string']
        },
        orgNotFound: {
            description: 'No such organization exists.'
        }
    },

    fn: async function (inputs, exits) {
        var org = await UserGroup.findOne(inputs.orgId);
        return exits.success(org);
    }

};