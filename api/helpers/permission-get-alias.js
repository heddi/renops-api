module.exports = {


  friendlyName: 'Permission Menu',


  description: 'Menampilkan alias menu untuk user login berdasarkan token',


  inputs: {
    INT_USRGRPID: {
      type: 'number',
      description: 'id group user',
      required: true
    }
  },


  exits: {

  },
  fn: async function (inputs, exits) {

    var permission = []
    //var sql = "SELECT a.*,b.INT_AUTHID,b.BIT_CREATE,b.BIT_EDIT,b.BIT_DELETE,b.BIT_VIEW,b.BIT_PRINT,b.BIT_MENU,BIT_CANCEL, BIT_APPROVE,BIT_EXCEL FROM MF_MODULE  a LEFT JOIN MF_AUTHORIZATION b ON a.INT_MODULEID=b.INT_MODULEID AND INT_USRGRPID= $1 WHERE a.INT_ACTIVE = 1";
    var sql = `SELECT 
				    a.*,
				    b.id idAuth,
				    b.idMenu,
				    b.bitCreate,
				    b.bitEdit,
				    b.bitDelete,
				    b.bitView,
				    b.bitMenu,
				    b.bitCancel,
				    b.bitApprove,
				    b.bitExcel
				FROM
				    menu a
				        LEFT JOIN
				    authorization b ON a.id = b.idMenu  AND idObject = `+inputs.INT_USRGRPID+`
				WHERE
				    a.active = 1 AND a.deleted=0`;
    //console.log('sql', sql);
    var rawResult = await sails.sendNativeQuery(sql);
    //return exits.success(rawResult);
    if (rawResult) {      
      async.eachSeries(rawResult.rows, function (elm, next) {
        //console.log(elm);
        if (elm['alias']) {

         
          if (elm['bitCreate'] == 1) {         
            permission.push('canCreate' + elm['alias']);
          }
          if (elm['bitEdit'] == 1) {
            permission.push('canEdit' + elm['alias']);
          }
          if (elm['bitDelete'] == 1) {
            permission.push('canDelete' + elm['alias']);
          }
          if (elm['bitView'] == 1) {
            permission.push('canView' + elm['alias']);
          }
          if (elm['bitPrint'] == 1) {
            permission.push('canPrint' + elm['alias']);
          }
          if (elm['bitCancel'] == 1) {
            permission.push('canCancel' + elm['alias']);
          }
          if (elm['bitApprove'] == 1) {
            permission.push('canApprove' + elm['alias']);
          }          
          if (elm['bitExcel'] == 1) {
            permission.push('canDownloadExcel' + elm['alias']);
          }

        }
        next();
      }, function allDone(err) {
        return exits.success(permission);
      });
    } else {
      return exits.success([]);
    }



  }


};

