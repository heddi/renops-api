module.exports = {
  friendlyName: 'Change Password',
  description: 'Ganti password user',
  inputs: {
   oldPassword: {
      type: 'string',
      description: 'Parameter oldpassword harus ada',
      required: true
   },
    newPassword: {
      type: 'string',
      description: 'Parameter newPassword harus ada',
      required: true
    }
  },
  exits: {
    success: {
      responseType: 'ok'
    },
    userNotFound: {
      description: `User tidak ditemukan dengan id ini`,
      responseType: `badRequest`
    },
    oldPasswordNotMatch: {
      description: `Password lama tidak sesuai`,
      responseType: `badRequest`
    }
  },


  fn: async function (inputs, exits) {
  	console.log('this.req.me',this.req.token.user);
  	var userRecord = await User.findOne(this.req.token.user.id);
    // If there was no matching user, respond thru the "badCombo" exit.
    if(!userRecord) {
      throw {userNotFound:"User tidak ditemukan"};
    }
    
  	await sails.helpers.passwords.checkPassword(inputs.oldPassword, userRecord.password)
    .intercept('incorrect', ()=>{ return {oldPasswordNotMatch:"Password lama tidak sesuai"};});
    
    // Hash the new password.
    var hashed = await sails.helpers.passwords.hashPassword(inputs.newPassword);

    // Update the record for the logged-in user.
    await User.update({ id: this.req.token.user.id })
    .set({
      password: hashed
    });
    

    return exits.success();

  }


};