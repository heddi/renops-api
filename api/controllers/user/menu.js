module.exports = {
  friendlyName: 'Menu',
  description: 'menampilkan list menu',
  inputs: {
  },
  exits: {
    success: {
      responseType: 'ok'
    },
    customRespon: {
      description: `Bebas deskripsi`,
      responseType: `notFound`
    }
  },


  fn: async function (inputs, exits) {
    MenuManager.treemenu(this.req.user.INT_USRGRPID, true, function (respon) {
      //console.log('respon',respon);
      return exits.success(respon);
    });
  }
};