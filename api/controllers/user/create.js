module.exports = {
  friendlyName: 'Create User',
  description: 'Buat user baru',
  inputs: {
    fullname: {
      type: 'string',
      required: true
    },
    username: {
      type: 'string',
      required: true
    },
    password: {
      type: 'string',
      required: true
    },
    userGroup: {
      type: 'number',
      required: true
    },
    active: {
      type: 'number',
      required: true
    },
  },
  exits: {
    success: {
      responseType: 'ok'
    },
    gagalInput: {
      description: `Gagal input user`,
      responseType: `badRequest`
    }
  },


  fn: async function (inputs, exits) {
  	inputs.password = await sails.helpers.passwords.hashPassword(inputs.password);
  	
	var createUser = await User.create(inputs).fetch();
	if(!createUser) {
      throw {gagalInput:"Gagal input user"};
    }
    return exits.success(createUser);

  }


};