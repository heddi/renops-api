module.exports = {


	friendlyName: 'Permission ',


	description: 'Mengambil alias hak akses untuk kebutuhan akses di frontend',


	inputs: {

	},


	exits: {
		success: {
			responseType: 'ok'
		},
		userGroupNotFound: {
			description: `Group user not found in database`,
			responseType: `badRequest`
		}

	},


	fn: async function (inputs, exits) {
		console.log('this.req.user', this.req.user);
		var userGroupObject = await UserGroup.findOne(this.req.user.userGroup);
		if (!userGroupObject) {
			throw {userGroupNotFound:"User group not found"};
		}
		var permission = await sails.helpers.permissionGetAlias(this.req.user.userGroup);
		//console.log('permission', permission);
		userGroupObject.permission = permission;
		return exits.success(userGroupObject);


	}


};
