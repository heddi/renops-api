module.exports = {
  friendlyName: 'Login',
  description: 'Log in using the provided username and password combination.',
  extendedDescription: ``,
  inputs: {
    tokenGoogle: {
      description: 'username login',
      type: 'string',
      required: true
    },
  },
  exits: {

    success: {
      description: 'The requesting user agent has been successfully logged in.',
      extendedDescription:
		`Under the covers, this stores the id of the logged-in user in the session
		as the \`userId\` key.  The next time this user agent sends a request, assuming
		it includes a cookie (like a web browser), Sails will automatically make this
		user id available as req.session.userId in the corresponding action.  (Also note
		that, thanks to the included "custom" hook, when a relevant request is received
		from a logged-in user, that user's entire record from the database will be fetched
		and exposed as \`req.me\`.)`
    },   
    usernameNotFound: {
      description: `The provided username  does not match any user in the database.`,
      responseType: 'unauthorized'
    },
    passwordNotFound: {
      description: `The provided password combination does not match to this user.`,
      responseType: 'unauthorized'
    },

  },


  fn: async function (inputs, exits) {
  	const CLIENT_ID = '418185088340-6c2lghjtbce9gitsqmpd88dokgtpqcqo.apps.googleusercontent.com';
  	const {OAuth2Client} = require('google-auth-library');
	const client = new OAuth2Client(CLIENT_ID);
	
	const ticket = await client.verifyIdToken({
	      idToken: inputs.tokenGoogle,
	      audience: CLIENT_ID,  
	});
	const payload = ticket.getPayload();
	console.log('payload',payload);
	const userid = payload['sub'];
	
    var userRecord = await User.findOne({
      email: payload.email,
    });

    // If there was no matching user, respond thru the "badCombo" exit.
    if(!userRecord) {
      throw {usernameNotFound:"The provided email  does not match any user in the database."};
    }
   

    // Modify the active session instance.
    this.req.session.userId = userRecord.id;

    // Send success response (this is where the session actually gets persisted)
    
    var token = await TokenManager.issueToken({user: userRecord});    
    var crypto = require('crypto');
	crypto.randomBytes(256, function (ex, buffer) {
    	if (ex) return ex;
    	var refreshToken = crypto.createHash('sha1').update(buffer).digest('hex');	    	
    	return exits.success({status:"success",token: token,"refreshToken":refreshToken});
  	});
    

  }

};
