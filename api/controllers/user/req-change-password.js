module.exports = {
  friendlyName: 'Request Change Password',
  description: 'User request change password by otp in email or phone',
  inputs: {
   email: {
      type: 'string',
      description: 'Parameter oldpassword harus ada'
   },
   phone: {
      type: 'string',
      description: 'Parameter newPassword harus ada'
   },
   via:{
   		type: 'string',
      	description: 'Parameter newPassword harus ada',
      	required:true
   }
  },
  exits: {
    success: {
      responseType: 'ok'
    },
    userNotFound: {
      description: `User tidak ditemukan dengan id ini`,
      responseType: `badRequest`
    },
  },


  fn: async function (inputs, exits) {
	var moment = require('moment');  	
  	if(inputs.via=='email'){
  		var foundUser = await User.findOne({email:inputs.email});
  		if(!foundUser) {
	      throw {userNotFound:"User tidak ditemukan dengan alamat email ini"};
	    }
	    
	    var OTP = Math.floor(1000 + Math.random() * 9000);	   
	    await User.update({email:inputs.email})
	    .set({
	      otp: OTP,
	      otpTime: moment().format("YYYY-MM-DD HH:mm:ss")
	    });
	    
	    var paramsEmail = {
	    	subjectMail: "Request New Passsword",
            to : foundUser.email,
            htmlBodyMail : 'Your OTP : '+ OTP
        }
        
        GlobalFunction.sendMail(paramsEmail, function(respon){
            console.log(respon);
        });

  	}
  	  	
    return exits.success("OTP SEND TO "+ inputs.via);

  }


};