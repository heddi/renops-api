module.exports = {
  friendlyName: 'Change Password',
  description: 'Ganti password user',
  inputs: {
   otp: {
      type: 'string',
      description: 'Parameter otp harus ada',
      required: true
   },
    newPassword: {
      type: 'string',
      description: 'Parameter newPassword harus ada',
      required: true
    }
  },
  exits: {
    success: {
      responseType: 'ok'
    },
    userNotFound: {
      description: `User tidak ditemukan dengan id ini`,
      responseType: `badRequest`
    },
    oldPasswordNotMatch: {
      description: `Password lama tidak sesuai`,
      responseType: `badRequest`
    }
  },


  fn: async function (inputs, exits) {
  	
  	var userRecord = await User.findOne({otp:inputs.otp});
    // If there was no matching user, respond thru the "badCombo" exit.
    if(!userRecord) {
      throw {userNotFound:"User tidak ditemukan"};
    }
    
    // Hash the new password.
    var hashed = await sails.helpers.passwords.hashPassword(inputs.newPassword);

    // Update the record for the logged-in user.
    await User.update({otp:inputs.otp})
    .set({
      password: hashed,
      otp:'',
    });
    

    return exits.success("Update password berhasil");

  }


};