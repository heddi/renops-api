module.exports = async function search(req, res) {
  var params = null;
  var label ='';
  if(req.param('kelompok')){
     label = 'kelompok'
     params = {deleted:0,'kelompok':{'contains':req.param('kelompok')}}
  }else if(req.param('sub_kelompok')){
    label = 'sub_kelompok'
    params = {deleted:0,'sub_kelompok': {'contains':req.param('sub_kelompok')}}
  }else if(req.param('uraian')){
    label = 'uraian'
    params = {deleted:0,'uraian': {'contains':req.param('uraian')}}
  }
  if(params){
    Katalog.find(params).exec(function(err, found) {
      if(err){
        return res.badRequest('error find value')
      }
      result = []
      found.forEach((itm, i) => {
        result.push(itm[label])
      })

      res.ok(Array.from(new Set(result)))

    });
  } else {
    res.badRequest('Error no Params')
  }
}
