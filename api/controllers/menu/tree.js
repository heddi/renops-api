module.exports = {
  friendlyName: 'Menu',
  description: 'menampilkan tree menu',
  inputs: {
    
  },
  exits: {
    success: {
      responseType: 'ok'
    },
  },


  fn: async function (inputs, exits) {

    MenuManager.treemenu(false, false, function (respon) {
        return exits.success(respon);
    });

  }


};