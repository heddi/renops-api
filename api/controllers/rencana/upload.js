module.exports = async function upload(req, res) {

  if (req.method === 'GET') return res.badRequest('GET not allowed')
  //var params = req.params.all()
  //console.log('req: ', req);
  //console.log('params: ', params);
  //console.log('req.file(file): ', req.file('file'));

  async.waterfall([createFile, saveToDb], function allDone(err, placeFile) {
    if (err) return res.badRequest('error upload file')

   return res.ok(placeFile)
  })

  function createFile(cb) {

    var pathFile = 'assets/sprint/';

    req.file('file').upload(
      {
        maxBytes: 1500000000,
        dirname: require('path').resolve(pathFile)
      },
      function whenUploadDone(err, uploadedFiles) {
       // console.log('uploadedFiles: ', uploadedFiles)

        if (err) {
          console.log('err: ', err)
          return cb(err)
        }
        var filename = uploadedFiles[0].fd

        return cb(null, filename)
      }
    )
  }

  function saveToDb(filename, cb) {
    var ID = req.param('id') || ''

    var oData = {
      file_sprint: filename.replace(/^.*[\\\/]/, '')
    }
// console.log('oData '+ ID,oData)
    if (ID) {
      Rencana.update({ id: req.param('id') }, oData, function(err, updatedRow) {
        if (err) {
          console.log('Rencana.update - err', err)
          return cb(err)
        }
        return cb(null, updatedRow)
      })
    } else {
      Rencana.find()
        .sort('id desc')
        .limit(1)
        .exec(function(err, foundTips) {
          if (err) return cb(err)

          Rencana.update({ id: foundTips[0].id }, oData, function(
            err,
            updatedRow
          ) {
            if (err) {
              console.log('Rencana.update - err', err)
              return cb(err)
            }
            return cb(null, updatedRow)
          })
        })
    }
  }
}
