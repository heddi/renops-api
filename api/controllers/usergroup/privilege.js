module.exports = {
    friendlyName: 'Update Privilege',
    description: 'Pengaturan hak akses untuk menu',
    inputs: {
        update: {
            type: 'ref',
            description: 'Create dan update menu baru ada disini',
        },
        remove: {
            type: 'ref',
            description: 'The current incoming request (params).',
        },
    },
    exits: {
        success: {
            responseType: 'ok'
        },
        customRespon: {
            description: `Bebas deskripsi`,
            responseType: `notFound`
        }
    },


    fn: async function (inputs, exits) {

        if (inputs.update) {
            console.log('inputs.update',inputs.update);
            _.each(inputs.update, function (group, moduleId) {
            	 console.log('inputs.update group',group);
                if (group.idAuth) {
                    var data4Insert = {
                        bitCreate: (group.bitCreate ? 1 : 0),
                        bitEdit: (group.bitEdit ? 1 : 0),
                        bitDelete: (group.bitDelete ? 1 : 0),
                        bitView: (group.bitView ? 1 : 0),
                        bitPrint: (group.bitPrint ? 1 : 0),
                        bitCancel: (group.bitCancel ? 1 : 0),
                        bitApprove: (group.bitApprove ? 1 : 0),
                        bitExcel: (group.bitExcel ? 1 : 0)
                    }
                    Authorization.update(group.idAuth, data4Insert).exec(function (err, done) {
                        if (err) console.log('err', err);
                        if (done) {
                            console.log('done update', done);
                        }
                    });
                    //console.log('data4Insert',data4Insert);
                    //console.log(group,moduleId)
                }
                //tambah baru
                if (group.id && !group.idAuth) {
                    var data4Insert = {
                        idObject: group.groupId,
                        menu: moduleId,
                        bitCreate: (group.bitCreate ? 1 : 0),
                        bitEdit: (group.bitEdit ? 1 : 0),
                        bitDelete: (group.bitDelete ? 1 : 0),
                        bitView: (group.bitView ? 1 : 0),
                        bitPrint: (group.bitPrint ? 1 : 0),
                        bitCancel: (group.bitCancel ? 1 : 0),
                        bitApprove: (group.bitApprove ? 1 : 0),
                        bitExcel: (group.bitExcel ? 1 : 0)
                    }
                    Authorization.create(data4Insert).exec(function (err, done) {
                        if (err) console.log('err', err);
                        if (done) {
                            //console.log('done create', done);
                        }
                    });

                }

            });
        }
        
        if (inputs.remove) {
            _.each(inputs.remove, function (row, idAuth) {            	
                if(idAuth){
                	 Authorization.destroy(idAuth).exec(function (err, deleted) {
	                    if (err) console.log('err', err);
	                    if (deleted) {
	                        //console.log('done deleted', deleted);
	                    }
	                });
                }
            });
        }
        
        return exits.success("update previledge");
    }
};