module.exports = {
    friendlyName: 'User Groups Privilege',
    description: 'Menampilkan list menu untuk hak akses ke user group',
    inputs: {
	   idUserGroup: {
	      type: 'number',
	      description: 'ID User Group',
	      required: true
	   },
	   isUser: {
	      type: 'number',
	      description: 'Parameter isUser digunakan untuk menampilkan menu sesuai login user',
	      required: true
	   },
    },
    exits: {
        success: {
            responseType: 'ok'
        }
    },


    fn: async function (inputs, exits) {
    	var idGroup = inputs.isUser ? this.req.user.userGroup : inputs.idUserGroup;
    	var showinmenu = inputs.isUser ? true : false;
        MenuManager.treemenu(idGroup, showinmenu, function (respon) {
            return exits.success(respon);
        });
    }
};