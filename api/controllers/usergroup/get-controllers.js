module.exports = {
  friendlyName: 'Get Controllers',
  description: 'Ambil semua controller untuk keperluan hak akses',
  inputs: {
    req: {
      type: 'ref',
      description: 'The current incoming request (req).',
    }
  },
  exits: {
    success: {
      responseType: 'ok'
    },
    customRespon: {
      description: `Bebas deskripsi`,
      responseType: `notFound`
    }
  },


  fn: async function (inputs, exits) {
    //console.log('sails.controllers', sails.getActions());
    return exits.success(sails.getActions());

  }


};