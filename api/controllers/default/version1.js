module.exports = {
  friendlyName: 'Menu',
  description: 'menampilkan list menu',
  inputs: {
    req: {
      type: 'ref',
      description: 'The current incoming request (req).',
      required: true
    },
    input1: {
      type: 'number',
      required: true
    }
  },
  exits: {
    success: {
      responseType: 'ok'
    },
    customRespon: {
      description: `Bebas deskripsi`,
      responseType: `notFound`
    }
  },


  fn: async function (inputs, exits) {
	console.log(inputs.req);
    return exits.success();

  }


};