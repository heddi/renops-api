-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 30, 2019 at 05:25 PM
-- Server version: 5.6.26
-- PHP Version: 5.5.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `renops`
--

-- --------------------------------------------------------

--
-- Table structure for table `authorization`
--

CREATE TABLE IF NOT EXISTS `authorization` (
  `id` int(11) NOT NULL,
  `idObject` int(11) NOT NULL COMMENT 'bisa id user atau id group user',
  `idMenu` int(11) NOT NULL,
  `bitCreate` int(1) DEFAULT '0',
  `bitEdit` int(1) DEFAULT '0',
  `bitDelete` int(1) DEFAULT '0',
  `bitView` int(1) DEFAULT '0',
  `bitPrint` int(1) DEFAULT '0',
  `bitMenu` int(1) DEFAULT '0',
  `bitCancel` int(1) DEFAULT '0',
  `bitApprove` int(1) DEFAULT '0',
  `bitExcel` int(11) DEFAULT '0',
  `deleted` int(11) DEFAULT '0',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=283 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `authorization`
--

INSERT INTO `authorization` (`id`, `idObject`, `idMenu`, `bitCreate`, `bitEdit`, `bitDelete`, `bitView`, `bitPrint`, `bitMenu`, `bitCancel`, `bitApprove`, `bitExcel`, `deleted`, `createdAt`, `updatedAt`) VALUES
(253, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, '2019-01-31 12:57:20', '2019-04-25 05:06:12'),
(254, 1, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2019-01-31 13:03:52', '2019-04-25 05:06:12'),
(255, 1, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2019-01-31 13:03:52', '2019-04-25 05:06:12'),
(256, 1, 9, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, '2019-01-31 13:03:52', '2019-04-25 05:06:12'),
(257, 1, 10, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, '2019-01-31 13:03:52', '2019-04-25 05:06:12'),
(260, 1, 46, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, '2019-01-31 13:22:46', '2019-04-25 05:06:12'),
(261, 1, 48, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, '2019-02-07 12:32:58', '2019-04-25 05:06:12'),
(262, 1, 49, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, '2019-02-08 07:51:24', '2019-04-25 05:06:12'),
(263, 1, 50, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, '2019-04-15 05:42:41', '2019-04-25 05:06:12'),
(264, 1, 51, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, '2019-04-15 05:46:32', '2019-04-25 05:06:12'),
(265, 0, 52, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, '2019-04-16 07:47:09', '2019-04-16 07:47:09'),
(266, 0, 52, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, '2019-04-16 09:53:36', '2019-04-16 09:53:36'),
(267, 1, 52, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, '2019-04-16 09:53:49', '2019-04-18 09:06:50'),
(268, 1, 53, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, '2019-04-18 08:00:55', '2019-04-18 09:06:50'),
(269, 1, 54, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, '2019-04-18 09:06:50', '2019-04-18 09:06:50'),
(270, 7, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, '2019-04-26 08:09:35', '2019-04-26 08:09:35'),
(271, 0, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2019-04-26 08:09:35', '2019-04-26 08:09:35'),
(272, 0, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2019-04-26 08:09:35', '2019-04-26 08:09:35'),
(273, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2019-04-26 08:09:35', '2019-04-26 08:09:35'),
(274, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2019-04-26 08:09:35', '2019-04-26 08:09:35'),
(275, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2019-04-26 08:09:35', '2019-04-26 08:09:35'),
(276, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2019-04-26 08:09:35', '2019-04-26 08:09:35'),
(277, 0, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2019-04-26 08:09:35', '2019-04-26 08:09:35'),
(278, 0, 48, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2019-04-26 08:09:35', '2019-04-26 08:09:35'),
(279, 0, 49, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2019-04-26 08:09:35', '2019-04-26 08:09:35'),
(280, 0, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2019-04-26 08:09:35', '2019-04-26 08:09:35'),
(281, 0, 53, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2019-04-26 08:09:35', '2019-04-26 08:09:35'),
(282, 0, 54, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2019-04-26 08:09:35', '2019-04-26 08:09:35');

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `id` int(11) NOT NULL,
  `groupConfig` varchar(45) DEFAULT NULL,
  `variable` varchar(45) DEFAULT NULL,
  `valueConfig` text,
  `valueType` varchar(45) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`id`, `groupConfig`, `variable`, `valueConfig`, `valueType`, `deleted`, `createdAt`, `updatedAt`) VALUES
(1, 'SMTP_MAIL', 'host', 'smtp.gmail.com', 'text', 0, '0000-00-00 00:00:00', '2019-02-08 07:14:07'),
(2, 'SMTP_MAIL', 'port', '465', 'text', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'SMTP_MAIL', 'user', 'aksimaya.testing@gmail.com', 'text', 0, '0000-00-00 00:00:00', '2019-02-08 07:13:31'),
(4, 'SMTP_MAIL', 'pass', '123qwe!@#QWE', 'password', 0, '0000-00-00 00:00:00', '2019-02-08 07:12:34');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL,
  `object` varchar(45) DEFAULT NULL,
  `action` varchar(45) DEFAULT NULL,
  `method` varchar(45) DEFAULT NULL,
  `params` text,
  `idUser` int(11) NOT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `object`, `action`, `method`, `params`, `idUser`, `fullname`, `deleted`, `createdAt`, `updatedAt`) VALUES
(1, '', 'menu/create', 'POST', '{"parent":"7","title":"Target Operasi","alias":"TargetList","path":"/operasi/target","icon":"","sorting":"7","active":"1"}', 29, 'admin2', 0, '2019-04-25 04:38:55', '2019-04-25 04:38:55'),
(2, '', 'menu/update', 'PUT', '{"id":"50","parent":"7","title":"Target Operasi","alias":"TargetList","path":"/operasi/target","icon":"fa fa-cogs","sorting":"7","active":"1"}', 29, 'admin2', 0, '2019-04-25 04:52:04', '2019-04-25 04:52:04'),
(3, '', 'target/update', 'PUT', '{"id":"4","jenis_data":"Data Terkait GIAT Kepolisians"}', 29, 'admin2', 0, '2019-04-25 04:53:43', '2019-04-25 04:53:43'),
(4, '', 'menu/create', 'POST', '{"parent":"7","title":"Jenis Operasi","alias":"MasterJenisList","path":"/operasi/jenis","icon":"fa-cogs","sorting":"7","active":"1"}', 29, 'admin2', 0, '2019-04-25 05:01:15', '2019-04-25 05:01:15'),
(5, '', 'menu/update', 'PUT', '{"id":"51","parent":"7","title":"Jenis Operasi","alias":"MasterJenisList","path":"/operasi/jenis","icon":"fa fa-cogs","sorting":"7","active":"1"}', 29, 'admin2', 0, '2019-04-25 05:01:25', '2019-04-25 05:01:25'),
(6, '', 'menu/update', 'PUT', '{"id":"50","parent":"7","title":"Target Operasi","alias":"MasterTargetList","path":"/operasi/target","icon":"fa fa-cogs","sorting":"7","active":"1"}', 29, 'admin2', 0, '2019-04-25 05:05:48', '2019-04-25 05:05:48'),
(7, '', 'usergroup/privilege', 'POST', '{"update":{"1":{"id":1,"title":"Dashboard","active":1,"parent":0,"path":"/dashboard/index","icon":"ft-home","alias":"Home","sorting":1,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-02-07T00:42:56.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":253,"idMenu":1,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]},"7":{"id":7,"title":"Master","active":1,"parent":0,"path":"","icon":"ft-folder","alias":"Master","sorting":2,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-02-07T00:15:51.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":254,"idMenu":7,"bitCreate":0,"bitEdit":0,"bitDelete":0,"bitView":0,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":0,"permission":1,"submenu":[{"id":8,"title":"User","active":1,"parent":7,"path":"","icon":"ft-user","alias":"MasterUser","sorting":1,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-03-04T01:44:02.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":255,"idMenu":8,"bitCreate":0,"bitEdit":0,"bitDelete":0,"bitView":0,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":0,"permission":1,"submenu":[{"id":9,"title":"User List","active":1,"parent":8,"path":"/user/list","icon":"","alias":"MasterUserList","sorting":1,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-01-30T21:31:49.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":256,"idMenu":9,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]},{"id":10,"title":"User Group","active":1,"parent":8,"path":"/usergroup","icon":"","alias":"MasterGroupList","sorting":2,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-01-30T21:35:24.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":257,"idMenu":10,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]}]},{"id":46,"title":"Menu Manager","active":1,"parent":7,"path":"/menu","icon":"fa fa-align-justify","alias":"MasterMenuManager","sorting":2,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-03-19T19:37:01.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":260,"idMenu":46,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]},{"id":48,"title":"Config","active":1,"parent":7,"path":"/config","icon":"fa fa-cogs","alias":"config","sorting":3,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-02-07T05:31:58.000Z","updatedAt":"2019-03-19T19:37:17.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":261,"idMenu":48,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]},{"id":49,"title":"Activity Logs","active":1,"parent":7,"path":"/activity-log","icon":"fa fa-bug","alias":"ActivityLogs","sorting":4,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-02-07T20:34:20.000Z","updatedAt":"2019-04-16T03:45:58.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":262,"idMenu":49,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]},{"id":50,"title":"Target Operasi","active":1,"parent":7,"path":"/operasi/target","icon":"fa fa-cogs","alias":"MasterTargetList","sorting":7,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-04-24T21:38:55.000Z","updatedAt":"2019-04-24T22:05:48.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":263,"idMenu":50,"bitCreate":true,"bitEdit":true,"bitDelete":true,"bitView":true,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":true,"permission":1,"submenu":[],"bitPrint":false},{"id":51,"title":"Jenis Operasi","active":1,"parent":7,"path":"/operasi/jenis","icon":"fa fa-cogs","alias":"MasterJenisList","sorting":7,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-04-24T22:01:15.000Z","updatedAt":"2019-04-24T22:01:25.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":264,"idMenu":51,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]}]},"8":{"id":8,"title":"User","active":1,"parent":7,"path":"","icon":"ft-user","alias":"MasterUser","sorting":1,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-03-04T01:44:02.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":255,"idMenu":8,"bitCreate":0,"bitEdit":0,"bitDelete":0,"bitView":0,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":0,"permission":1,"submenu":[{"id":9,"title":"User List","active":1,"parent":8,"path":"/user/list","icon":"","alias":"MasterUserList","sorting":1,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-01-30T21:31:49.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":256,"idMenu":9,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]},{"id":10,"title":"User Group","active":1,"parent":8,"path":"/usergroup","icon":"","alias":"MasterGroupList","sorting":2,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-01-30T21:35:24.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":257,"idMenu":10,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]}]},"9":{"id":9,"title":"User List","active":1,"parent":8,"path":"/user/list","icon":"","alias":"MasterUserList","sorting":1,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-01-30T21:31:49.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":256,"idMenu":9,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]},"10":{"id":10,"title":"User Group","active":1,"parent":8,"path":"/usergroup","icon":"","alias":"MasterGroupList","sorting":2,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-01-30T21:35:24.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":257,"idMenu":10,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]},"46":{"id":46,"title":"Menu Manager","active":1,"parent":7,"path":"/menu","icon":"fa fa-align-justify","alias":"MasterMenuManager","sorting":2,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-03-19T19:37:01.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":260,"idMenu":46,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]},"48":{"id":48,"title":"Config","active":1,"parent":7,"path":"/config","icon":"fa fa-cogs","alias":"config","sorting":3,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-02-07T05:31:58.000Z","updatedAt":"2019-03-19T19:37:17.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":261,"idMenu":48,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]},"49":{"id":49,"title":"Activity Logs","active":1,"parent":7,"path":"/activity-log","icon":"fa fa-bug","alias":"ActivityLogs","sorting":4,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-02-07T20:34:20.000Z","updatedAt":"2019-04-16T03:45:58.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":262,"idMenu":49,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]},"50":{"id":50,"title":"Target Operasi","active":1,"parent":7,"path":"/operasi/target","icon":"fa fa-cogs","alias":"MasterTargetList","sorting":7,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-04-24T21:38:55.000Z","updatedAt":"2019-04-24T22:05:48.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":263,"idMenu":50,"bitCreate":true,"bitEdit":true,"bitDelete":true,"bitView":true,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":true,"permission":1,"submenu":[],"bitPrint":false},"51":{"id":51,"title":"Jenis Operasi","active":1,"parent":7,"path":"/operasi/jenis","icon":"fa fa-cogs","alias":"MasterJenisList","sorting":7,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-04-24T22:01:15.000Z","updatedAt":"2019-04-24T22:01:25.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":264,"idMenu":51,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]}},"remove":{},"add":{}}', 29, 'admin2', 0, '2019-04-25 05:06:12', '2019-04-25 05:06:12'),
(8, '', 'user/update', 'PUT', '{"id":"29","fullname":"Admin Backup","email":"admin@admin","username":"admin2","userGroup":"1","active":"1"}', 1, 'admin', 0, '2019-04-25 05:32:08', '2019-04-25 05:32:08'),
(9, '', 'menu/create', 'POST', '{"parent":"7","title":"Polda","alias":"MasterPolda","path":"/polda/list","icon":"fa fa-cogs","sorting":"9","active":"1"}', 1, 'admin', 0, '2019-04-26 02:46:32', '2019-04-26 02:46:32'),
(10, '', 'menu/update', 'PUT', '{"id":"52","parent":"7","title":"Polda","alias":"MasterPoldaList","path":"/polda/list","icon":"fa fa-cogs","sorting":"9","active":"1"}', 1, 'admin', 0, '2019-04-26 03:00:16', '2019-04-26 03:00:16'),
(11, '', 'menu/create', 'POST', '{"parent":"7","title":"Polres","alias":"MasterPolresList","path":"/polres/list","icon":"fa fa-cogs","sorting":"9","active":"1"}', 1, 'admin', 0, '2019-04-26 03:11:11', '2019-04-26 03:11:11'),
(12, '', 'jenis/create', 'POST', '{"jenis_operasi":"xxx"}', 1, 'admin', 0, '2019-04-26 04:28:09', '2019-04-26 04:28:09'),
(13, '', 'jenis/update', 'PUT', '{"id":"6","deleted":"1"}', 1, 'admin', 0, '2019-04-26 04:28:15', '2019-04-26 04:28:15'),
(14, '', 'target/create', 'POST', '{"jenis_data":"ca"}', 1, 'admin', 0, '2019-04-26 04:28:34', '2019-04-26 04:28:34'),
(15, '', 'target/update', 'PUT', '{"id":"5","deleted":"1"}', 1, 'admin', 0, '2019-04-26 04:28:44', '2019-04-26 04:28:44'),
(16, '', 'jenis/create', 'POST', '{"jenis_operasi":"ops"}', 1, 'admin', 0, '2019-04-26 04:28:50', '2019-04-26 04:28:50'),
(17, '', 'jenis/update', 'PUT', '{"id":"7","deleted":"1"}', 1, 'admin', 0, '2019-04-26 04:28:55', '2019-04-26 04:28:55'),
(18, '', 'polda/create', 'POST', '{"nama_polda":"Jawa Barat","wilayah_hukum":"Jawa Barat"}', 1, 'admin', 0, '2019-04-26 04:31:59', '2019-04-26 04:31:59'),
(19, '', 'polda/update', 'PUT', '{"id":"9","nama_polda":"Polda Jabar","wilayah_hukum":"Jawa Barat"}', 1, 'admin', 0, '2019-04-26 04:33:57', '2019-04-26 04:33:57'),
(20, '', 'polres/create', 'POST', '{"nama_polres":"Polres Bogor","polda_id":"9"}', 1, 'admin', 0, '2019-04-26 04:34:21', '2019-04-26 04:34:21'),
(21, '', 'menu/create', 'POST', '{"title":"Rencana Operasi ","alias":"RencanaOperasi","path":"/renops","icon":"fa fa-align-justify","sorting":"1","active":"1"}', 1, 'admin', 0, '2019-04-26 04:40:37', '2019-04-26 04:40:37'),
(22, '', 'user/create', 'POST', '{"fullname":"polres_bogor","email":"polres_bogor@email.com","username":"polres_bogor","userGroup":"7","password":"12345","active":"1"}', 1, 'admin', 0, '2019-04-26 08:09:20', '2019-04-26 08:09:20'),
(23, '', 'usergroup/privilege', 'POST', '{"update":{"1":{"id":1,"title":"Dashboard","active":1,"parent":0,"path":"/dashboard/index","icon":"ft-home","alias":"Home","sorting":1,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-02-07T00:42:56.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":null,"idMenu":null,"bitCreate":null,"bitEdit":null,"bitDelete":null,"bitView":true,"bitMenu":null,"bitCancel":null,"bitApprove":null,"bitExcel":null,"permission":0,"submenu":[],"groupId":"7"},"7":{"id":7,"title":"Master","active":1,"parent":0,"path":"","icon":"ft-folder","alias":"Master","sorting":2,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-02-07T00:15:51.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":null,"idMenu":null,"bitCreate":null,"bitEdit":null,"bitDelete":null,"bitView":null,"bitMenu":null,"bitCancel":null,"bitApprove":null,"bitExcel":null,"permission":0,"submenu":[{"id":46,"title":"Menu Manager","active":1,"parent":7,"path":"/menu","icon":"fa fa-align-justify","alias":"MasterMenuManager","sorting":2,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-03-19T19:37:01.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":null,"idMenu":null,"bitCreate":null,"bitEdit":null,"bitDelete":null,"bitView":null,"bitMenu":null,"bitCancel":null,"bitApprove":null,"bitExcel":null,"permission":0,"submenu":[]},{"id":48,"title":"Config","active":1,"parent":7,"path":"/config","icon":"fa fa-cogs","alias":"config","sorting":3,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-02-07T05:31:58.000Z","updatedAt":"2019-03-19T19:37:17.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":null,"idMenu":null,"bitCreate":null,"bitEdit":null,"bitDelete":null,"bitView":null,"bitMenu":null,"bitCancel":null,"bitApprove":null,"bitExcel":null,"permission":0,"submenu":[]},{"id":49,"title":"Activity Logs","active":1,"parent":7,"path":"/activity-log","icon":"fa fa-bug","alias":"ActivityLogs","sorting":4,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-02-07T20:34:20.000Z","updatedAt":"2019-04-16T03:45:58.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":null,"idMenu":null,"bitCreate":null,"bitEdit":null,"bitDelete":null,"bitView":null,"bitMenu":null,"bitCancel":null,"bitApprove":null,"bitExcel":null,"permission":0,"submenu":[]},{"id":51,"title":"Jenis Operasi","active":1,"parent":7,"path":"/operasi/jenis","icon":"fa fa-cogs","alias":"MasterJenisList","sorting":7,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-04-24T22:01:15.000Z","updatedAt":"2019-04-24T22:01:25.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":null,"idMenu":null,"bitCreate":null,"bitEdit":null,"bitDelete":null,"bitView":null,"bitMenu":null,"bitCancel":null,"bitApprove":null,"bitExcel":null,"permission":0,"submenu":[]},{"id":50,"title":"Target Operasi","active":1,"parent":7,"path":"/operasi/target","icon":"fa fa-cogs","alias":"MasterTargetList","sorting":7,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-04-24T21:38:55.000Z","updatedAt":"2019-04-24T22:05:48.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":null,"idMenu":null,"bitCreate":null,"bitEdit":null,"bitDelete":null,"bitView":null,"bitMenu":null,"bitCancel":null,"bitApprove":null,"bitExcel":null,"permission":0,"submenu":[]},{"id":52,"title":"Polda","active":1,"parent":7,"path":"/polda/list","icon":"fa fa-cogs","alias":"MasterPoldaList","sorting":9,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-04-25T19:46:32.000Z","updatedAt":"2019-04-25T20:00:16.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":null,"idMenu":null,"bitCreate":null,"bitEdit":null,"bitDelete":null,"bitView":null,"bitMenu":null,"bitCancel":null,"bitApprove":null,"bitExcel":null,"permission":0,"submenu":[]},{"id":53,"title":"Polres","active":1,"parent":7,"path":"/polres/list","icon":"fa fa-cogs","alias":"MasterPolresList","sorting":10,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-04-25T20:11:11.000Z","updatedAt":"2019-04-25T20:11:11.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":null,"idMenu":null,"bitCreate":null,"bitEdit":null,"bitDelete":null,"bitView":null,"bitMenu":null,"bitCancel":null,"bitApprove":null,"bitExcel":null,"permission":0,"submenu":[]},{"id":8,"title":"User","active":1,"parent":7,"path":"","icon":"ft-user","alias":"MasterUser","sorting":11,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-03-04T01:44:02.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":null,"idMenu":null,"bitCreate":null,"bitEdit":null,"bitDelete":null,"bitView":null,"bitMenu":null,"bitCancel":null,"bitApprove":null,"bitExcel":null,"permission":0,"submenu":[{"id":9,"title":"User List","active":1,"parent":8,"path":"/user/list","icon":"","alias":"MasterUserList","sorting":1,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-01-30T21:31:49.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":null,"idMenu":null,"bitCreate":null,"bitEdit":null,"bitDelete":null,"bitView":null,"bitMenu":null,"bitCancel":null,"bitApprove":null,"bitExcel":null,"permission":0,"submenu":[]},{"id":10,"title":"User Group","active":1,"parent":8,"path":"/usergroup","icon":"","alias":"MasterGroupList","sorting":2,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-01-30T21:35:24.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":null,"idMenu":null,"bitCreate":null,"bitEdit":null,"bitDelete":null,"bitView":null,"bitMenu":null,"bitCancel":null,"bitApprove":null,"bitExcel":null,"permission":0,"submenu":[]}]}]},"8":{"id":8,"title":"User","active":1,"parent":7,"path":"","icon":"ft-user","alias":"MasterUser","sorting":11,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-03-04T01:44:02.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":null,"idMenu":null,"bitCreate":null,"bitEdit":null,"bitDelete":null,"bitView":null,"bitMenu":null,"bitCancel":null,"bitApprove":null,"bitExcel":null,"permission":0,"submenu":[{"id":9,"title":"User List","active":1,"parent":8,"path":"/user/list","icon":"","alias":"MasterUserList","sorting":1,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-01-30T21:31:49.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":null,"idMenu":null,"bitCreate":null,"bitEdit":null,"bitDelete":null,"bitView":null,"bitMenu":null,"bitCancel":null,"bitApprove":null,"bitExcel":null,"permission":0,"submenu":[]},{"id":10,"title":"User Group","active":1,"parent":8,"path":"/usergroup","icon":"","alias":"MasterGroupList","sorting":2,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-01-30T21:35:24.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":null,"idMenu":null,"bitCreate":null,"bitEdit":null,"bitDelete":null,"bitView":null,"bitMenu":null,"bitCancel":null,"bitApprove":null,"bitExcel":null,"permission":0,"submenu":[]}]},"9":{"id":9,"title":"User List","active":1,"parent":8,"path":"/user/list","icon":"","alias":"MasterUserList","sorting":1,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-01-30T21:31:49.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":null,"idMenu":null,"bitCreate":null,"bitEdit":null,"bitDelete":null,"bitView":null,"bitMenu":null,"bitCancel":null,"bitApprove":null,"bitExcel":null,"permission":0,"submenu":[]},"10":{"id":10,"title":"User Group","active":1,"parent":8,"path":"/usergroup","icon":"","alias":"MasterGroupList","sorting":2,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-01-30T21:35:24.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":null,"idMenu":null,"bitCreate":null,"bitEdit":null,"bitDelete":null,"bitView":null,"bitMenu":null,"bitCancel":null,"bitApprove":null,"bitExcel":null,"permission":0,"submenu":[]},"46":{"id":46,"title":"Menu Manager","active":1,"parent":7,"path":"/menu","icon":"fa fa-align-justify","alias":"MasterMenuManager","sorting":2,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-03-19T19:37:01.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":null,"idMenu":null,"bitCreate":null,"bitEdit":null,"bitDelete":null,"bitView":null,"bitMenu":null,"bitCancel":null,"bitApprove":null,"bitExcel":null,"permission":0,"submenu":[]},"48":{"id":48,"title":"Config","active":1,"parent":7,"path":"/config","icon":"fa fa-cogs","alias":"config","sorting":3,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-02-07T05:31:58.000Z","updatedAt":"2019-03-19T19:37:17.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":null,"idMenu":null,"bitCreate":null,"bitEdit":null,"bitDelete":null,"bitView":null,"bitMenu":null,"bitCancel":null,"bitApprove":null,"bitExcel":null,"permission":0,"submenu":[]},"49":{"id":49,"title":"Activity Logs","active":1,"parent":7,"path":"/activity-log","icon":"fa fa-bug","alias":"ActivityLogs","sorting":4,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-02-07T20:34:20.000Z","updatedAt":"2019-04-16T03:45:58.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":null,"idMenu":null,"bitCreate":null,"bitEdit":null,"bitDelete":null,"bitView":null,"bitMenu":null,"bitCancel":null,"bitApprove":null,"bitExcel":null,"permission":0,"submenu":[]},"50":{"id":50,"title":"Target Operasi","active":1,"parent":7,"path":"/operasi/target","icon":"fa fa-cogs","alias":"MasterTargetList","sorting":7,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-04-24T21:38:55.000Z","updatedAt":"2019-04-24T22:05:48.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":null,"idMenu":null,"bitCreate":null,"bitEdit":null,"bitDelete":null,"bitView":null,"bitMenu":null,"bitCancel":null,"bitApprove":null,"bitExcel":null,"permission":0,"submenu":[]},"51":{"id":51,"title":"Jenis Operasi","active":1,"parent":7,"path":"/operasi/jenis","icon":"fa fa-cogs","alias":"MasterJenisList","sorting":7,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-04-24T22:01:15.000Z","updatedAt":"2019-04-24T22:01:25.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":null,"idMenu":null,"bitCreate":null,"bitEdit":null,"bitDelete":null,"bitView":null,"bitMenu":null,"bitCancel":null,"bitApprove":null,"bitExcel":null,"permission":0,"submenu":[]},"52":{"id":52,"title":"Polda","active":1,"parent":7,"path":"/polda/list","icon":"fa fa-cogs","alias":"MasterPoldaList","sorting":9,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-04-25T19:46:32.000Z","updatedAt":"2019-04-25T20:00:16.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":null,"idMenu":null,"bitCreate":null,"bitEdit":null,"bitDelete":null,"bitView":null,"bitMenu":null,"bitCancel":null,"bitApprove":null,"bitExcel":null,"permission":0,"submenu":[]},"53":{"id":53,"title":"Polres","active":1,"parent":7,"path":"/polres/list","icon":"fa fa-cogs","alias":"MasterPolresList","sorting":10,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-04-25T20:11:11.000Z","updatedAt":"2019-04-25T20:11:11.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":null,"idMenu":null,"bitCreate":null,"bitEdit":null,"bitDelete":null,"bitView":null,"bitMenu":null,"bitCancel":null,"bitApprove":null,"bitExcel":null,"permission":0,"submenu":[]},"54":{"id":54,"title":"Rencana Operasi ","active":1,"parent":0,"path":"/renops/list","icon":"fa fa-align-justify","alias":"RencanaOperasi","sorting":1,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-04-25T21:40:37.000Z","updatedAt":"2019-04-25T21:40:37.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":null,"idMenu":null,"bitCreate":null,"bitEdit":null,"bitDelete":null,"bitView":null,"bitMenu":null,"bitCancel":null,"bitApprove":null,"bitExcel":null,"permission":0,"submenu":[]}},"remove":{},"add":{}}', 1, 'admin', 0, '2019-04-26 08:09:35', '2019-04-26 08:09:35'),
(24, '', 'rencana/create', 'POST', '{"year":2019,"jenisOperasiId":"1","noOperasi":"xc","tanggalMulai":"","tanggalAkhir":"","totalPelaksanaan":5,"catatan":"zczc","statusRenops":"D","statusOperasi":"W","fileSprint":""}', 1, 'admin', 0, '2019-04-29 09:43:08', '2019-04-29 09:43:08'),
(25, '', 'rencana/create', 'POST', '{"year":2019,"jenisOperasiId":"1","noOperasi":"xc","tanggalMulai":"","tanggalAkhir":"","totalPelaksanaan":5,"catatan":"zczc","statusRenops":"D","statusOperasi":"W","fileSprint":{}}', 1, 'admin', 0, '2019-04-29 09:43:42', '2019-04-29 09:43:42'),
(26, '', 'rencana/create', 'POST', '{"tahun":2019,"jenis_operasi_id":"3","no_operasi":"xx/asas/asas","tgl_mulai":"","tgl_akhir":"","total_pelaksanaan":5,"catatan":"catatan si booooy","status_renops":"D","status_operasi":"W","file_sprint":{}}', 1, 'admin', 0, '2019-04-29 09:51:53', '2019-04-29 09:51:53'),
(27, '', 'rencana/create', 'POST', '{"file":""}', 1, 'admin', 0, '2019-04-29 09:53:27', '2019-04-29 09:53:27'),
(28, '', 'rencana/create', 'POST', '{}', 1, 'admin', 0, '2019-04-29 09:56:18', '2019-04-29 09:56:18'),
(29, '', 'rencana/create', 'POST', '{"file":""}', 1, 'admin', 0, '2019-04-30 01:56:04', '2019-04-30 01:56:04'),
(30, '', 'rencana/create', 'POST', '{"tahun":"2019","jenis_operasi_id":"2","no_operasi":"xax/aad/23424","tgl_mulai":"","tgl_akhir":"","total_pelaksanaan":"5","catatan":"vxvxv","status_renops":"D","status_operasi":"W","file_sprint":"[object Object]"}', 1, 'admin', 0, '2019-04-30 02:20:03', '2019-04-30 02:20:03'),
(31, '', 'rencana/new', 'POST', '{"tahun":"2019","jenis_operasi_id":"1","no_operasi":"xax/aad/23424","tgl_mulai":"","tgl_akhir":"","total_pelaksanaan":"5","catatan":"xxxxx","status_renops":"D","status_operasi":"W","file_sprint":"[object Object]"}', 1, 'admin', 0, '2019-04-30 02:47:27', '2019-04-30 02:47:27'),
(32, '', 'rencana/new', 'POST', '{"tahun":"2019","jenis_operasi_id":"1","no_operasi":"xax/aad/23424","tgl_mulai":"","tgl_akhir":"","total_pelaksanaan":"5","catatan":"xxxxx","status_renops":"D","status_operasi":"W","file_sprint":"[object Object]"}', 1, 'admin', 0, '2019-04-30 02:48:36', '2019-04-30 02:48:36'),
(33, '', 'rencana/new', 'POST', '{"tahun":"2019","jenis_operasi_id":"1","no_operasi":"xax/aad/23424","tgl_mulai":"","tgl_akhir":"","total_pelaksanaan":"5","catatan":"xxxxx","status_renops":"D","status_operasi":"W","file_sprint":"[object Object]"}', 1, 'admin', 0, '2019-04-30 02:51:22', '2019-04-30 02:51:22'),
(34, '', 'rencana/new', 'POST', '{"tahun":"2019","jenis_operasi_id":"1","no_operasi":"xax/aad/23424","tgl_mulai":"","tgl_akhir":"","total_pelaksanaan":"5","catatan":"xxxxx","status_renops":"D","status_operasi":"W","file_sprint":"[object Object]"}', 1, 'admin', 0, '2019-04-30 02:54:25', '2019-04-30 02:54:25'),
(35, '', 'rencana/new', 'POST', '{"tahun":"2019","jenis_operasi_id":"1","no_operasi":"xax/aad/23424","tgl_mulai":"","tgl_akhir":"","total_pelaksanaan":"5","catatan":"xxxxx","status_renops":"D","status_operasi":"W","file_sprint":"[object Object]"}', 1, 'admin', 0, '2019-04-30 02:55:30', '2019-04-30 02:55:30'),
(36, '', 'rencana/new', 'POST', '{"tahun":"2019","jenis_operasi_id":"1","no_operasi":"xax/aad/23424","tgl_mulai":"","tgl_akhir":"","total_pelaksanaan":"5","catatan":"xxxxx","status_renops":"D","status_operasi":"W","file_sprint":"[object Object]"}', 1, 'admin', 0, '2019-04-30 02:59:51', '2019-04-30 02:59:51'),
(37, '', 'rencana/new', 'POST', '{"tahun":"2019","jenis_operasi_id":"1","no_operasi":"xax/aad/23424","tgl_mulai":"","tgl_akhir":"","total_pelaksanaan":"5","catatan":"xxxxx","status_renops":"D","status_operasi":"W","file_sprint":"[object Object]"}', 1, 'admin', 0, '2019-04-30 03:02:43', '2019-04-30 03:02:43'),
(38, '', 'rencana/new', 'POST', '{"tahun":"2019","jenis_operasi_id":"1","no_operasi":"xax/aad/23424","tgl_mulai":"","tgl_akhir":"","total_pelaksanaan":"5","catatan":"xxxxx","status_renops":"D","status_operasi":"W","file_sprint":"[object Object]"}', 1, 'admin', 0, '2019-04-30 03:04:50', '2019-04-30 03:04:50'),
(39, '', 'rencana/new', 'POST', '{"tahun":"2019","jenis_operasi_id":"1","no_operasi":"xax/aad/23424","tgl_mulai":"","tgl_akhir":"","total_pelaksanaan":"5","catatan":"xxxxx","status_renops":"D","status_operasi":"W","file_sprint":"[object Object]"}', 1, 'admin', 0, '2019-04-30 03:22:32', '2019-04-30 03:22:32'),
(40, '', 'rencana/new', 'POST', '{"tahun":"2019","jenis_operasi_id":"1","no_operasi":"sfsfsfx/afw3d/23424","tgl_mulai":"","tgl_akhir":"","total_pelaksanaan":"5","catatan":"adadczczc","status_renops":"D","status_operasi":"W","file_sprint":"[object Object]"}', 1, 'admin', 0, '2019-04-30 07:14:04', '2019-04-30 07:14:04'),
(41, '', 'rencana/new', 'POST', '{"tahun":"2019","jenis_operasi_id":"1","no_operasi":"sfsfsfx/afw3d/23424","tgl_mulai":"","tgl_akhir":"","total_pelaksanaan":"5","catatan":"adadczczc","status_renops":"D","status_operasi":"W","file_sprint":"[object Object]"}', 1, 'admin', 0, '2019-04-30 07:17:12', '2019-04-30 07:17:12'),
(42, '', 'rencana/new', 'POST', '{"tahun":"2019","jenis_operasi_id":"1","no_operasi":"sfsfsfx/afw3d/23424","tgl_mulai":"","tgl_akhir":"","total_pelaksanaan":"5","catatan":"adadczczc","status_renops":"D","status_operasi":"W","file_sprint":"[object Object]"}', 1, 'admin', 0, '2019-04-30 07:18:19', '2019-04-30 07:18:19'),
(43, '', 'rencana/new', 'POST', '{"tahun":"2019","jenis_operasi_id":"1","no_operasi":"sfsfsfx/afw3d/23424","tgl_mulai":"","tgl_akhir":"","total_pelaksanaan":"5","catatan":"adadczczc","status_renops":"D","status_operasi":"W","file_sprint":"[object Object]"}', 1, 'admin', 0, '2019-04-30 07:19:51', '2019-04-30 07:19:51'),
(44, '', 'rencana/new', 'POST', '{"tahun":"2019","jenis_operasi_id":"1","no_operasi":"sfsfsfx/afw3d/23424","tgl_mulai":"","tgl_akhir":"","total_pelaksanaan":"5","catatan":"adadczczc","status_renops":"D","status_operasi":"W","file_sprint":"[object Object]"}', 1, 'admin', 0, '2019-04-30 07:20:40', '2019-04-30 07:20:40'),
(45, '', 'rencana/new', 'POST', '{"tahun":"2019","jenis_operasi_id":"1","no_operasi":"sfsfsfx/afw3d/23424","tgl_mulai":"","tgl_akhir":"","total_pelaksanaan":"5","catatan":"adadczczc","status_renops":"D","status_operasi":"W","file_sprint":"[object Object]"}', 1, 'admin', 0, '2019-04-30 07:21:26', '2019-04-30 07:21:26'),
(46, '', 'rencana/new', 'POST', '{"tahun":"2019","jenis_operasi_id":"1","no_operasi":"sfsfsfx/afw3d/23424","tgl_mulai":"","tgl_akhir":"","total_pelaksanaan":"5","catatan":"adadczczc","status_renops":"D","status_operasi":"W","file_sprint":"[object Object]"}', 1, 'admin', 0, '2019-04-30 07:23:55', '2019-04-30 07:23:55'),
(47, '', 'rencana/new', 'POST', '{"tahun":"2019","jenis_operasi_id":"1","no_operasi":"sfsfsfx/afw3d/23424","tgl_mulai":"","tgl_akhir":"","total_pelaksanaan":"5","catatan":"adadczczc","status_renops":"D","status_operasi":"W","file_sprint":"[object Object]"}', 1, 'admin', 0, '2019-04-30 07:25:55', '2019-04-30 07:25:55'),
(48, '', 'rencana/new', 'POST', '{"tahun":"2019","jenis_operasi_id":"1","no_operasi":"sfsfsfx/afw3d/23424","tgl_mulai":"","tgl_akhir":"","total_pelaksanaan":"5","catatan":"adadczczc","status_renops":"D","status_operasi":"W","file_sprint":"[object Object]"}', 1, 'admin', 0, '2019-04-30 07:27:55', '2019-04-30 07:27:55'),
(49, '', 'rencana/create', 'POST', '{"tahun":2019,"jenis_operasi_id":"2","no_operasi":"sfsfsfx/afw3d/23424","tgl_mulai":"2019-04-02T17:00:00.000Z","tgl_akhir":"2019-04-25T17:00:00.000Z","total_pelaksanaan":5,"catatan":"adadafsf","status_renops":"D","status_operasi":"W"}', 1, 'admin', 0, '2019-04-30 09:47:55', '2019-04-30 09:47:55'),
(50, '', 'rencana/create', 'POST', '{"tahun":2019,"jenis_operasi_id":"2","no_operasi":"sfsfsfx/afw3d/23424","tgl_mulai":"2019-04-02T17:00:00.000Z","tgl_akhir":"2019-04-25T17:00:00.000Z","total_pelaksanaan":5,"catatan":"adadafsf","status_renops":"D","status_operasi":"W"}', 1, 'admin', 0, '2019-04-30 09:51:03', '2019-04-30 09:51:03'),
(51, '', 'rencana/create', 'POST', '{"tahun":2019,"jenis_operasi_id":"2","no_operasi":"sfsfsfx/afw3d/23424","tgl_mulai":"2019-04-02T17:00:00.000Z","tgl_akhir":"2019-04-25T17:00:00.000Z","total_pelaksanaan":5,"catatan":"adadafsf","status_renops":"D","status_operasi":"W"}', 1, 'admin', 0, '2019-04-30 09:51:55', '2019-04-30 09:51:55'),
(52, '', 'rencana/create', 'POST', '{"tahun":2019,"jenis_operasi_id":"2","no_operasi":"sfsfsfx/afw3d/23424","tgl_mulai":"2019-04-02T17:00:00.000Z","tgl_akhir":"2019-04-25T17:00:00.000Z","total_pelaksanaan":5,"catatan":"adadafsf","status_renops":"D","status_operasi":"W"}', 1, 'admin', 0, '2019-04-30 09:53:00', '2019-04-30 09:53:00'),
(53, '', 'rencana/create', 'POST', '{"tahun":2019,"jenis_operasi_id":"2","no_operasi":"sfsfsfx/afw3d/23424","tgl_mulai":"2019-04-02T17:00:00.000Z","tgl_akhir":"2019-04-25T17:00:00.000Z","total_pelaksanaan":5,"catatan":"adadafsf","status_renops":"D","status_operasi":"W"}', 1, 'admin', 0, '2019-04-30 09:54:00', '2019-04-30 09:54:00'),
(54, '', 'rencana/create', 'POST', '{"tahun":2019,"jenis_operasi_id":"4","no_operasi":"xx/asas/asas","tgl_mulai":"2019-05-26T17:00:00.000Z","tgl_akhir":"2019-06-06T17:00:00.000Z","total_pelaksanaan":"20","catatan":"lebarraan.. lebaran","status_renops":"D","status_operasi":"R"}', 1, 'admin', 0, '2019-04-30 09:56:27', '2019-04-30 09:56:27'),
(55, '', 'rencana/create', 'POST', '{"tahun":2019,"jenis_operasi_id":"2","no_operasi":"afafa/235/3334","tgl_mulai":"2019-04-16T17:00:00.000Z","tgl_akhir":"2019-04-22T17:00:00.000Z","total_pelaksanaan":"3535","catatan":"qdad","status_renops":"D","status_operasi":"W"}', 1, 'admin', 0, '2019-04-30 10:00:21', '2019-04-30 10:00:21'),
(56, '', 'rencana/create', 'POST', '{"tahun":2019,"jenis_operasi_id":"2","no_operasi":"afafa/235/3334","tgl_mulai":"2019-04-16T17:00:00.000Z","tgl_akhir":"2019-04-22T17:00:00.000Z","total_pelaksanaan":"3535","catatan":"qdad","status_renops":"D","status_operasi":"W"}', 1, 'admin', 0, '2019-04-30 10:01:18', '2019-04-30 10:01:18'),
(57, '', 'rencana/create', 'POST', '{"tahun":2019,"jenis_operasi_id":"3","no_operasi":"afafa/235/3334","tgl_mulai":"2019-04-18T17:00:00.000Z","tgl_akhir":"2019-04-25T17:00:00.000Z","total_pelaksanaan":5,"catatan":"sgsg","status_renops":"D","status_operasi":"W"}', 1, 'admin', 0, '2019-04-30 10:02:06', '2019-04-30 10:02:06'),
(58, '', 'rencana/create', 'POST', '{"tahun":2019,"jenis_operasi_id":"3","no_operasi":"afafa/235/3334","tgl_mulai":"2019-04-10T17:00:00.000Z","tgl_akhir":"2019-04-03T17:00:00.000Z","total_pelaksanaan":"3","catatan":"sfsf","status_renops":"D","status_operasi":"W"}', 1, 'admin', 0, '2019-04-30 10:04:23', '2019-04-30 10:04:23'),
(59, '', 'rencana/create', 'POST', '{"tahun":2019,"jenis_operasi_id":"3","no_operasi":"afafa/235/3334","tgl_mulai":"2019-04-10T17:00:00.000Z","tgl_akhir":"2019-04-03T17:00:00.000Z","total_pelaksanaan":"3","catatan":"sfsf","status_renops":"D","status_operasi":"W"}', 1, 'admin', 0, '2019-04-30 10:06:29', '2019-04-30 10:06:29'),
(60, '', 'rencana/upload', 'POST', '{"tahun":"2019","jenis_operasi_id":"3","no_operasi":"afafa/235/3334","tgl_mulai":"Thu Apr 11 2019 00:00:00 GMT 0700 (Indochina Time)","tgl_akhir":"Thu Apr 04 2019 00:00:00 GMT 0700 (Indochina Time)","total_pelaksanaan":"3","catatan":"sfsf","status_renops":"D","status_operasi":"W"}', 1, 'admin', 0, '2019-04-30 10:06:30', '2019-04-30 10:06:30'),
(61, '', 'rencana/create', 'POST', '{"tahun":2019,"jenis_operasi_id":"2","no_operasi":"xax/aad/23424","tgl_mulai":"2019-04-18T17:00:00.000Z","tgl_akhir":"2019-04-16T17:00:00.000Z","total_pelaksanaan":5,"catatan":"adad","status_renops":"D","status_operasi":"W"}', 1, 'admin', 0, '2019-04-30 10:07:25', '2019-04-30 10:07:25'),
(62, '', 'rencana/upload', 'POST', '{"tahun":"2019","jenis_operasi_id":"2","no_operasi":"xax/aad/23424","tgl_mulai":"Fri Apr 19 2019 00:00:00 GMT 0700 (Indochina Time)","tgl_akhir":"Wed Apr 17 2019 00:00:00 GMT 0700 (Indochina Time)","total_pelaksanaan":"5","catatan":"adad","status_renops":"D","status_operasi":"W"}', 1, 'admin', 0, '2019-04-30 10:07:25', '2019-04-30 10:07:25'),
(63, '', 'rencana/create', 'POST', '{"tahun":2019,"jenis_operasi_id":"2","no_operasi":"xax/aad/23424","tgl_mulai":"2019-04-18T17:00:00.000Z","tgl_akhir":"2019-04-16T17:00:00.000Z","total_pelaksanaan":5,"catatan":"sf","status_renops":"D","status_operasi":"W"}', 1, 'admin', 0, '2019-04-30 10:09:20', '2019-04-30 10:09:20'),
(64, '', 'rencana/upload', 'POST', '{"id":"19"}', 1, 'admin', 0, '2019-04-30 10:09:20', '2019-04-30 10:09:20'),
(65, '', 'rencana/create', 'POST', '{"tahun":2019,"jenis_operasi_id":"2","no_operasi":"xax/aad/23424","tgl_mulai":"2019-04-18T17:00:00.000Z","tgl_akhir":"2019-04-16T17:00:00.000Z","total_pelaksanaan":5,"catatan":"sf","status_renops":"D","status_operasi":"W"}', 1, 'admin', 0, '2019-04-30 10:10:09', '2019-04-30 10:10:09'),
(66, '', 'rencana/upload', 'POST', '{"id":"20"}', 1, 'admin', 0, '2019-04-30 10:10:09', '2019-04-30 10:10:09'),
(67, '', 'rencana/create', 'POST', '{"tahun":2019,"jenis_operasi_id":"2","no_operasi":"xax/aad/23424","tgl_mulai":"2019-04-18T17:00:00.000Z","tgl_akhir":"2019-04-16T17:00:00.000Z","total_pelaksanaan":5,"catatan":"sf","status_renops":"D","status_operasi":"W"}', 1, 'admin', 0, '2019-04-30 10:18:37', '2019-04-30 10:18:37'),
(68, '', 'rencana/create', 'POST', '{"tahun":2019,"jenis_operasi_id":"2","no_operasi":"xax/aad/23424","tgl_mulai":"2019-04-18T17:00:00.000Z","tgl_akhir":"2019-04-16T17:00:00.000Z","total_pelaksanaan":5,"catatan":"sf","status_renops":"D","status_operasi":"W"}', 1, 'admin', 0, '2019-04-30 10:18:37', '2019-04-30 10:18:37'),
(69, '', 'rencana/upload', 'POST', '{"id":"21"}', 1, 'admin', 0, '2019-04-30 10:18:37', '2019-04-30 10:18:37'),
(70, '', 'rencana/upload', 'POST', '{"id":"22"}', 1, 'admin', 0, '2019-04-30 10:18:37', '2019-04-30 10:18:37'),
(71, '', 'rencana/create', 'POST', '{"tahun":2019,"jenis_operasi_id":"2","no_operasi":"xax/aad/23424","tgl_mulai":"2019-04-18T17:00:00.000Z","tgl_akhir":"2019-04-16T17:00:00.000Z","total_pelaksanaan":5,"catatan":"sf","status_renops":"D","status_operasi":"W"}', 1, 'admin', 0, '2019-04-30 10:18:46', '2019-04-30 10:18:46'),
(72, '', 'rencana/upload', 'POST', '{"id":"23"}', 1, 'admin', 0, '2019-04-30 10:18:47', '2019-04-30 10:18:47'),
(73, '', 'rencana/create', 'POST', '{"tahun":2019,"jenis_operasi_id":"2","no_operasi":"xax/aad/23424","tgl_mulai":"2019-04-18T17:00:00.000Z","tgl_akhir":"2019-04-16T17:00:00.000Z","total_pelaksanaan":5,"catatan":"sf","status_renops":"D","status_operasi":"W"}', 1, 'admin', 0, '2019-04-30 10:19:54', '2019-04-30 10:19:54'),
(74, '', 'rencana/upload', 'POST', '{"id":"24"}', 1, 'admin', 0, '2019-04-30 10:19:54', '2019-04-30 10:19:54'),
(75, '', 'rencana/create', 'POST', '{"tahun":2019,"jenis_operasi_id":"2","no_operasi":"xax/aad/23424","tgl_mulai":"2019-04-18T17:00:00.000Z","tgl_akhir":"2019-04-16T17:00:00.000Z","total_pelaksanaan":5,"catatan":"sf","status_renops":"D","status_operasi":"W"}', 1, 'admin', 0, '2019-04-30 10:21:12', '2019-04-30 10:21:12'),
(76, '', 'rencana/upload', 'POST', '{"id":"25"}', 1, 'admin', 0, '2019-04-30 10:21:13', '2019-04-30 10:21:13'),
(77, '', 'rencana/create', 'POST', '{"tahun":2019,"jenis_operasi_id":"2","no_operasi":"xax/aad/23424","tgl_mulai":"2019-04-18T17:00:00.000Z","tgl_akhir":"2019-04-16T17:00:00.000Z","total_pelaksanaan":5,"catatan":"sf","status_renops":"D","status_operasi":"W"}', 1, 'admin', 0, '2019-04-30 10:21:59', '2019-04-30 10:21:59'),
(78, '', 'rencana/upload', 'POST', '{"id":"26"}', 1, 'admin', 0, '2019-04-30 10:21:59', '2019-04-30 10:21:59');

-- --------------------------------------------------------

--
-- Table structure for table `md_katalog_target_operasi`
--

CREATE TABLE IF NOT EXISTS `md_katalog_target_operasi` (
  `id` int(11) unsigned NOT NULL,
  `target_operasi_id` int(11) DEFAULT NULL,
  `kelompok` varchar(50) DEFAULT NULL,
  `sub_kelompok` varchar(50) DEFAULT NULL,
  `uraian` varchar(100) DEFAULT NULL,
  `keterangan_operasi_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `md_keterangan_operasi`
--

CREATE TABLE IF NOT EXISTS `md_keterangan_operasi` (
  `id` int(11) unsigned NOT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `md_keterangan_operasi`
--

INSERT INTO `md_keterangan_operasi` (`id`, `keterangan`, `createdAt`, `updatedAt`, `deleted`) VALUES
(1, 'Perkara', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(2, 'Unit', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(3, 'Orang', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(4, 'Buah', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(5, 'Lokasi', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(6, 'Rp', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(7, 'Kali', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `md_polda`
--

CREATE TABLE IF NOT EXISTS `md_polda` (
  `id` int(11) unsigned NOT NULL,
  `nama_polda` varchar(30) DEFAULT NULL,
  `wilayah_hukum` varchar(50) DEFAULT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `md_polda`
--

INSERT INTO `md_polda` (`id`, `nama_polda`, `wilayah_hukum`, `createdAt`, `updatedAt`, `deleted`) VALUES
(1, 'Polda Aceh', 'Aceh', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(2, 'Polda Sumut', 'Sumatera Utara', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(3, 'Polda Sumbar', 'Sumatera Barat', '2019-04-25 15:18:05', '2019-04-25 15:18:05', 0),
(4, 'Polda Jambi', 'Jambi', '2019-04-25 15:18:05', '2019-04-25 15:18:05', 0),
(5, 'Polda Riau', ' Riau', '2019-04-25 15:18:26', '2019-04-25 15:18:26', 0),
(6, 'Polda Kepri', 'Kepulauan Riau', '2019-04-25 15:18:26', '2019-04-25 15:18:26', 0),
(7, 'Polda Babel', 'Kepulauan Bangka Belitung', '2019-04-25 15:18:50', '2019-04-25 15:18:50', 0),
(8, 'Polda Sumsel', 'Sumatera Selatan', '2019-04-25 15:18:50', '2019-04-25 15:18:50', 0),
(9, 'Polda Jabar', 'Jawa Barat', '2019-04-26 04:31:59', '2019-04-26 04:33:57', 0);

-- --------------------------------------------------------

--
-- Table structure for table `md_polres`
--

CREATE TABLE IF NOT EXISTS `md_polres` (
  `id` int(11) unsigned NOT NULL,
  `polda_id` int(11) DEFAULT NULL,
  `nama_polres` varchar(50) DEFAULT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `md_polres`
--

INSERT INTO `md_polres` (`id`, `polda_id`, `nama_polres`, `createdAt`, `updatedAt`, `deleted`) VALUES
(1, 1, 'Aceh Jaya', '2019-04-25 00:00:00', '2019-04-25 00:00:00', 0),
(2, 1, 'Aceh Utara', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(3, 9, 'Polres Bogor', '2019-04-26 04:34:21', '2019-04-26 04:34:21', 0);

-- --------------------------------------------------------

--
-- Table structure for table `md_rencana_operasi`
--

CREATE TABLE IF NOT EXISTS `md_rencana_operasi` (
  `id` int(11) unsigned NOT NULL,
  `tahun` char(4) DEFAULT NULL,
  `jenis_operasi_id` int(11) DEFAULT NULL,
  `no_operasi` varchar(30) DEFAULT NULL,
  `tgl_mulai` datetime DEFAULT NULL,
  `tgl_akhir` datetime DEFAULT NULL,
  `total_pelaksanaan` smallint(2) DEFAULT NULL,
  `catatan` varchar(255) DEFAULT NULL,
  `file_sprint` varchar(100) DEFAULT NULL,
  `status_renops` char(1) DEFAULT 'D' COMMENT 'D: Draft, P:Publish',
  `status_operasi` char(1) DEFAULT 'W' COMMENT 'W:Waiting, R: Running',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `md_rencana_operasi`
--

INSERT INTO `md_rencana_operasi` (`id`, `tahun`, `jenis_operasi_id`, `no_operasi`, `tgl_mulai`, `tgl_akhir`, `total_pelaksanaan`, `catatan`, `file_sprint`, `status_renops`, `status_operasi`, `createdAt`, `updatedAt`, `deleted`) VALUES
(26, '2019', 2, 'xax/aad/23424', '2019-04-18 17:00:00', '2019-04-16 17:00:00', 5, 'sf', '5345499f-d0e9-4303-849e-639039a3ce72.pdf', 'D', 'W', '2019-04-30 10:21:59', '2019-04-30 10:21:59', 0);

-- --------------------------------------------------------

--
-- Table structure for table `md_target_operasi`
--

CREATE TABLE IF NOT EXISTS `md_target_operasi` (
  `id` int(11) unsigned NOT NULL,
  `jenis_data` varchar(100) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `md_target_operasi`
--

INSERT INTO `md_target_operasi` (`id`, `jenis_data`, `createdAt`, `updatedAt`, `deleted`) VALUES
(1, 'Data Terkait Masalah Pelanggaran Lalu Lintas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(2, 'Data Terkait Masalah Kecelakaan Lalu Lintas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(3, 'Data Terkait DIKMAS Lantas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(4, 'Data Terkait GIAT Kepolisians', '0000-00-00 00:00:00', '2019-04-25 04:53:43', 0),
(5, 'ca', '2019-04-26 04:28:34', '2019-04-26 04:28:44', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `active` tinyint(4) DEFAULT '1',
  `parent` int(11) NOT NULL DEFAULT '0',
  `path` varchar(45) DEFAULT NULL,
  `icon` varchar(45) DEFAULT NULL,
  `alias` varchar(100) NOT NULL,
  `sorting` int(11) NOT NULL DEFAULT '0',
  `className` varchar(45) DEFAULT NULL,
  `badge` varchar(45) DEFAULT NULL,
  `badgeClass` varchar(45) DEFAULT NULL,
  `isExternalLink` varchar(45) DEFAULT NULL,
  `isNavHeader` varchar(45) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `authAction` varchar(200) DEFAULT '["bitCreate","bitEdit","bitDelete","bitView","bitPrint","bitCancel","bitApprove","bitExcel"]'
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `title`, `active`, `parent`, `path`, `icon`, `alias`, `sorting`, `className`, `badge`, `badgeClass`, `isExternalLink`, `isNavHeader`, `deleted`, `createdAt`, `updatedAt`, `authAction`) VALUES
(1, 'Dashboard', 1, 0, '/dashboard/index', 'ft-home', 'Home', 1, '', NULL, NULL, '', '', 0, '0000-00-00 00:00:00', '2019-02-07 07:42:56', '["bitCreate","bitEdit","bitDelete","bitView","bitPrint","bitCancel","bitApprove","bitExcel"]'),
(7, 'Master', 1, 0, '', 'ft-folder', 'Master', 2, NULL, NULL, NULL, '', '', 0, '0000-00-00 00:00:00', '2019-02-07 07:15:51', '["bitCreate","bitEdit","bitDelete","bitView","bitPrint","bitCancel","bitApprove","bitExcel"]'),
(8, 'User', 1, 7, '', 'ft-user', 'MasterUser', 11, NULL, NULL, NULL, '', '', 0, '0000-00-00 00:00:00', '2019-03-04 08:44:02', '["bitCreate","bitEdit","bitDelete","bitView","bitPrint","bitCancel","bitApprove","bitExcel"]'),
(9, 'User List', 1, 8, '/user/list', '', 'MasterUserList', 1, '', NULL, NULL, '', '', 0, '0000-00-00 00:00:00', '2019-01-31 04:31:49', '["bitCreate","bitEdit","bitDelete","bitView","bitPrint","bitCancel","bitApprove","bitExcel"]'),
(10, 'User Group', 1, 8, '/usergroup', '', 'MasterGroupList', 2, '', NULL, NULL, '', '', 0, '0000-00-00 00:00:00', '2019-01-31 04:35:24', '["bitCreate","bitEdit","bitDelete","bitView","bitPrint","bitCancel","bitApprove","bitExcel"]'),
(46, 'Menu Manager', 1, 7, '/menu', 'fa fa-align-justify', 'MasterMenuManager', 2, '', NULL, NULL, '', '', 0, '0000-00-00 00:00:00', '2019-03-20 02:37:01', '["bitCreate","bitEdit","bitDelete","bitView","bitPrint","bitCancel","bitApprove","bitExcel"]'),
(48, 'Config', 1, 7, '/config', 'fa fa-cogs', 'config', 3, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-07 12:31:58', '2019-03-20 02:37:17', '["bitCreate","bitEdit","bitDelete","bitView","bitPrint","bitCancel","bitApprove","bitExcel"]'),
(49, 'Activity Logs', 1, 7, '/activity-log', 'fa fa-bug', 'ActivityLogs', 4, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-08 03:34:20', '2019-04-16 10:45:58', '["bitCreate","bitEdit","bitDelete","bitView","bitPrint","bitCancel","bitApprove","bitExcel"]'),
(50, 'Target Operasi', 1, 7, '/operasi/target', 'fa fa-cogs', 'MasterTargetList', 7, NULL, NULL, NULL, NULL, NULL, 0, '2019-04-25 04:38:55', '2019-04-25 05:05:48', '["bitCreate","bitEdit","bitDelete","bitView","bitPrint","bitCancel","bitApprove","bitExcel"]'),
(51, 'Jenis Operasi', 1, 7, '/operasi/jenis', 'fa fa-cogs', 'MasterJenisList', 7, NULL, NULL, NULL, NULL, NULL, 0, '2019-04-25 05:01:15', '2019-04-25 05:01:25', '["bitCreate","bitEdit","bitDelete","bitView","bitPrint","bitCancel","bitApprove","bitExcel"]'),
(52, 'Polda', 1, 7, '/polda/list', 'fa fa-cogs', 'MasterPoldaList', 9, NULL, NULL, NULL, NULL, NULL, 0, '2019-04-26 02:46:32', '2019-04-26 03:00:16', '["bitCreate","bitEdit","bitDelete","bitView","bitPrint","bitCancel","bitApprove","bitExcel"]'),
(53, 'Polres', 1, 7, '/polres/list', 'fa fa-cogs', 'MasterPolresList', 10, NULL, NULL, NULL, NULL, NULL, 0, '2019-04-26 03:11:11', '2019-04-26 03:11:11', '["bitCreate","bitEdit","bitDelete","bitView","bitPrint","bitCancel","bitApprove","bitExcel"]'),
(54, 'Rencana Operasi ', 1, 0, '/renops/list', 'fa fa-align-justify', 'RencanaOperasi', 1, NULL, NULL, NULL, NULL, NULL, 0, '2019-04-26 04:40:37', '2019-04-26 04:40:37', '["bitCreate","bitEdit","bitDelete","bitView","bitPrint","bitCancel","bitApprove","bitExcel"]');

-- --------------------------------------------------------

--
-- Table structure for table `ref_jenis_operasi`
--

CREATE TABLE IF NOT EXISTS `ref_jenis_operasi` (
  `id` int(11) unsigned NOT NULL,
  `jenis_operasi` varchar(30) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_jenis_operasi`
--

INSERT INTO `ref_jenis_operasi` (`id`, `jenis_operasi`, `createdAt`, `updatedAt`, `deleted`) VALUES
(1, 'Operasi Zebra', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(2, 'Operasi Lilin', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(3, 'Operasi Patuh', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(4, 'Operasi Ketupat', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(5, 'Operasi Simpatik', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(6, 'xxx', '2019-04-26 04:28:09', '2019-04-26 04:28:15', 1),
(7, 'ops', '2019-04-26 04:28:50', '2019-04-26 04:28:55', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ref_jenis_user`
--

CREATE TABLE IF NOT EXISTS `ref_jenis_user` (
  `id` int(11) unsigned NOT NULL,
  `jenis_user` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_jenis_user`
--

INSERT INTO `ref_jenis_user` (`id`, `jenis_user`) VALUES
(1, 'MABES'),
(2, 'POLDA'),
(3, 'POLRES');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL COMMENT 'ID User',
  `fullname` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL COMMENT 'Nama User',
  `password` varchar(100) NOT NULL COMMENT 'Password',
  `email` varchar(45) NOT NULL,
  `otp` varchar(45) DEFAULT NULL,
  `otpTime` datetime NOT NULL,
  `idUserGroup` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  `lastlogin` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `createdBy` int(11) DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `fullname`, `username`, `password`, `email`, `otp`, `otpTime`, `idUserGroup`, `active`, `lastlogin`, `deleted`, `createdBy`, `updatedBy`, `createdAt`, `updatedAt`) VALUES
(1, 'Admin', 'admin', '$2a$10$AZXIDaRjSsZr6/Q2h0jKc.FqDo1A2cJnNW1.LAMew1u179YKQAvi.', '', NULL, '0000-00-00 00:00:00', 1, 1, NULL, 0, NULL, NULL, '2018-02-02 20:52:57', '2018-06-04 20:52:57'),
(29, 'Admin Backup', 'admin2', '$2a$10$AZXIDaRjSsZr6/Q2h0jKc.FqDo1A2cJnNW1.LAMew1u179YKQAvi.', 'admin@admin', NULL, '0000-00-00 00:00:00', 1, 1, NULL, 0, NULL, NULL, '0000-00-00 00:00:00', '2019-04-25 05:32:08'),
(38, 'polres_bogor', 'polres_bogor', '$2a$10$g05qX5ZhnGQzIp6xIjMAwOOAdpECcwlXH712iw/7m.Tlu.pml7K5m', '', NULL, '0000-00-00 00:00:00', 7, 1, NULL, 0, NULL, NULL, '2019-04-26 08:09:20', '2019-04-26 08:09:20');

-- --------------------------------------------------------

--
-- Table structure for table `usertoken`
--

CREATE TABLE IF NOT EXISTS `usertoken` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `token` text NOT NULL,
  `refreshToken` text NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE IF NOT EXISTS `user_group` (
  `id` int(11) NOT NULL,
  `groupName` varchar(45) DEFAULT NULL,
  `apiCrud` varchar(45) DEFAULT '["POST","PUT","GET","DELETE"]',
  `apiActionDisabled` text,
  `createdBy` int(11) DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`id`, `groupName`, `apiCrud`, `apiActionDisabled`, `createdBy`, `updatedBy`, `deleted`, `createdAt`, `updatedAt`) VALUES
(1, 'Administrator', '["POST","PUT","GET","DELETE"]', '{}', NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'POLDA', '["POST","PUT","GET","DELETE"]', '{}', NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'POLRES', '["POST","PUT","GET","DELETE"]', '{}', NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'MABES', '["POST","PUT","GET","DELETE"]', '{}', NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authorization`
--
ALTER TABLE `authorization`
  ADD PRIMARY KEY (`id`),
  ADD KEY `groupAndModule` (`idMenu`,`idObject`),
  ADD KEY `idxObject` (`idObject`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `md_katalog_target_operasi`
--
ALTER TABLE `md_katalog_target_operasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `md_keterangan_operasi`
--
ALTER TABLE `md_keterangan_operasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `md_polda`
--
ALTER TABLE `md_polda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `md_polres`
--
ALTER TABLE `md_polres`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `md_rencana_operasi`
--
ALTER TABLE `md_rencana_operasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `md_target_operasi`
--
ALTER TABLE `md_target_operasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `active` (`active`);

--
-- Indexes for table `ref_jenis_operasi`
--
ALTER TABLE `ref_jenis_operasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ref_jenis_user`
--
ALTER TABLE `ref_jenis_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `VCH_USERNAME_UNIQUE` (`username`);

--
-- Indexes for table `usertoken`
--
ALTER TABLE `usertoken`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `authorization`
--
ALTER TABLE `authorization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=283;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `md_katalog_target_operasi`
--
ALTER TABLE `md_katalog_target_operasi`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `md_keterangan_operasi`
--
ALTER TABLE `md_keterangan_operasi`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `md_polda`
--
ALTER TABLE `md_polda`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `md_polres`
--
ALTER TABLE `md_polres`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `md_rencana_operasi`
--
ALTER TABLE `md_rencana_operasi`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `md_target_operasi`
--
ALTER TABLE `md_target_operasi`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `ref_jenis_operasi`
--
ALTER TABLE `ref_jenis_operasi`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ref_jenis_user`
--
ALTER TABLE `ref_jenis_user`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID User',AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `usertoken`
--
ALTER TABLE `usertoken`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_group`
--
ALTER TABLE `user_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
