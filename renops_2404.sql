-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 25, 2019 at 12:34 PM
-- Server version: 5.6.26
-- PHP Version: 5.5.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `renops`
--

-- --------------------------------------------------------

--
-- Table structure for table `authorization`
--

CREATE TABLE IF NOT EXISTS `authorization` (
  `id` int(11) NOT NULL,
  `idObject` int(11) NOT NULL COMMENT 'bisa id user atau id group user',
  `idMenu` int(11) NOT NULL,
  `bitCreate` int(1) DEFAULT '0',
  `bitEdit` int(1) DEFAULT '0',
  `bitDelete` int(1) DEFAULT '0',
  `bitView` int(1) DEFAULT '0',
  `bitPrint` int(1) DEFAULT '0',
  `bitMenu` int(1) DEFAULT '0',
  `bitCancel` int(1) DEFAULT '0',
  `bitApprove` int(1) DEFAULT '0',
  `bitExcel` int(11) DEFAULT '0',
  `deleted` int(11) DEFAULT '0',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=270 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `authorization`
--

INSERT INTO `authorization` (`id`, `idObject`, `idMenu`, `bitCreate`, `bitEdit`, `bitDelete`, `bitView`, `bitPrint`, `bitMenu`, `bitCancel`, `bitApprove`, `bitExcel`, `deleted`, `createdAt`, `updatedAt`) VALUES
(253, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, '2019-01-31 12:57:20', '2019-04-25 05:06:12'),
(254, 1, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2019-01-31 13:03:52', '2019-04-25 05:06:12'),
(255, 1, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2019-01-31 13:03:52', '2019-04-25 05:06:12'),
(256, 1, 9, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, '2019-01-31 13:03:52', '2019-04-25 05:06:12'),
(257, 1, 10, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, '2019-01-31 13:03:52', '2019-04-25 05:06:12'),
(260, 1, 46, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, '2019-01-31 13:22:46', '2019-04-25 05:06:12'),
(261, 1, 48, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, '2019-02-07 12:32:58', '2019-04-25 05:06:12'),
(262, 1, 49, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, '2019-02-08 07:51:24', '2019-04-25 05:06:12'),
(263, 1, 50, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, '2019-04-15 05:42:41', '2019-04-25 05:06:12'),
(264, 1, 51, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, '2019-04-15 05:46:32', '2019-04-25 05:06:12'),
(265, 0, 52, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, '2019-04-16 07:47:09', '2019-04-16 07:47:09'),
(266, 0, 52, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, '2019-04-16 09:53:36', '2019-04-16 09:53:36'),
(267, 1, 52, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, '2019-04-16 09:53:49', '2019-04-18 09:06:50'),
(268, 1, 53, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, '2019-04-18 08:00:55', '2019-04-18 09:06:50'),
(269, 1, 54, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, '2019-04-18 09:06:50', '2019-04-18 09:06:50');

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `id` int(11) NOT NULL,
  `groupConfig` varchar(45) DEFAULT NULL,
  `variable` varchar(45) DEFAULT NULL,
  `valueConfig` text,
  `valueType` varchar(45) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`id`, `groupConfig`, `variable`, `valueConfig`, `valueType`, `deleted`, `createdAt`, `updatedAt`) VALUES
(1, 'SMTP_MAIL', 'host', 'smtp.gmail.com', 'text', 0, '0000-00-00 00:00:00', '2019-02-08 07:14:07'),
(2, 'SMTP_MAIL', 'port', '465', 'text', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'SMTP_MAIL', 'user', 'aksimaya.testing@gmail.com', 'text', 0, '0000-00-00 00:00:00', '2019-02-08 07:13:31'),
(4, 'SMTP_MAIL', 'pass', '123qwe!@#QWE', 'password', 0, '0000-00-00 00:00:00', '2019-02-08 07:12:34');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL,
  `object` varchar(45) DEFAULT NULL,
  `action` varchar(45) DEFAULT NULL,
  `method` varchar(45) DEFAULT NULL,
  `params` text,
  `idUser` int(11) NOT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `object`, `action`, `method`, `params`, `idUser`, `fullname`, `deleted`, `createdAt`, `updatedAt`) VALUES
(1, '', 'menu/create', 'POST', '{"parent":"7","title":"Target Operasi","alias":"TargetList","path":"/operasi/target","icon":"","sorting":"7","active":"1"}', 29, 'admin2', 0, '2019-04-25 04:38:55', '2019-04-25 04:38:55'),
(2, '', 'menu/update', 'PUT', '{"id":"50","parent":"7","title":"Target Operasi","alias":"TargetList","path":"/operasi/target","icon":"fa fa-cogs","sorting":"7","active":"1"}', 29, 'admin2', 0, '2019-04-25 04:52:04', '2019-04-25 04:52:04'),
(3, '', 'target/update', 'PUT', '{"id":"4","jenis_data":"Data Terkait GIAT Kepolisians"}', 29, 'admin2', 0, '2019-04-25 04:53:43', '2019-04-25 04:53:43'),
(4, '', 'menu/create', 'POST', '{"parent":"7","title":"Jenis Operasi","alias":"MasterJenisList","path":"/operasi/jenis","icon":"fa-cogs","sorting":"7","active":"1"}', 29, 'admin2', 0, '2019-04-25 05:01:15', '2019-04-25 05:01:15'),
(5, '', 'menu/update', 'PUT', '{"id":"51","parent":"7","title":"Jenis Operasi","alias":"MasterJenisList","path":"/operasi/jenis","icon":"fa fa-cogs","sorting":"7","active":"1"}', 29, 'admin2', 0, '2019-04-25 05:01:25', '2019-04-25 05:01:25'),
(6, '', 'menu/update', 'PUT', '{"id":"50","parent":"7","title":"Target Operasi","alias":"MasterTargetList","path":"/operasi/target","icon":"fa fa-cogs","sorting":"7","active":"1"}', 29, 'admin2', 0, '2019-04-25 05:05:48', '2019-04-25 05:05:48'),
(7, '', 'usergroup/privilege', 'POST', '{"update":{"1":{"id":1,"title":"Dashboard","active":1,"parent":0,"path":"/dashboard/index","icon":"ft-home","alias":"Home","sorting":1,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-02-07T00:42:56.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":253,"idMenu":1,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]},"7":{"id":7,"title":"Master","active":1,"parent":0,"path":"","icon":"ft-folder","alias":"Master","sorting":2,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-02-07T00:15:51.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":254,"idMenu":7,"bitCreate":0,"bitEdit":0,"bitDelete":0,"bitView":0,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":0,"permission":1,"submenu":[{"id":8,"title":"User","active":1,"parent":7,"path":"","icon":"ft-user","alias":"MasterUser","sorting":1,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-03-04T01:44:02.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":255,"idMenu":8,"bitCreate":0,"bitEdit":0,"bitDelete":0,"bitView":0,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":0,"permission":1,"submenu":[{"id":9,"title":"User List","active":1,"parent":8,"path":"/user/list","icon":"","alias":"MasterUserList","sorting":1,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-01-30T21:31:49.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":256,"idMenu":9,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]},{"id":10,"title":"User Group","active":1,"parent":8,"path":"/usergroup","icon":"","alias":"MasterGroupList","sorting":2,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-01-30T21:35:24.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":257,"idMenu":10,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]}]},{"id":46,"title":"Menu Manager","active":1,"parent":7,"path":"/menu","icon":"fa fa-align-justify","alias":"MasterMenuManager","sorting":2,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-03-19T19:37:01.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":260,"idMenu":46,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]},{"id":48,"title":"Config","active":1,"parent":7,"path":"/config","icon":"fa fa-cogs","alias":"config","sorting":3,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-02-07T05:31:58.000Z","updatedAt":"2019-03-19T19:37:17.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":261,"idMenu":48,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]},{"id":49,"title":"Activity Logs","active":1,"parent":7,"path":"/activity-log","icon":"fa fa-bug","alias":"ActivityLogs","sorting":4,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-02-07T20:34:20.000Z","updatedAt":"2019-04-16T03:45:58.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":262,"idMenu":49,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]},{"id":50,"title":"Target Operasi","active":1,"parent":7,"path":"/operasi/target","icon":"fa fa-cogs","alias":"MasterTargetList","sorting":7,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-04-24T21:38:55.000Z","updatedAt":"2019-04-24T22:05:48.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":263,"idMenu":50,"bitCreate":true,"bitEdit":true,"bitDelete":true,"bitView":true,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":true,"permission":1,"submenu":[],"bitPrint":false},{"id":51,"title":"Jenis Operasi","active":1,"parent":7,"path":"/operasi/jenis","icon":"fa fa-cogs","alias":"MasterJenisList","sorting":7,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-04-24T22:01:15.000Z","updatedAt":"2019-04-24T22:01:25.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":264,"idMenu":51,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]}]},"8":{"id":8,"title":"User","active":1,"parent":7,"path":"","icon":"ft-user","alias":"MasterUser","sorting":1,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-03-04T01:44:02.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":255,"idMenu":8,"bitCreate":0,"bitEdit":0,"bitDelete":0,"bitView":0,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":0,"permission":1,"submenu":[{"id":9,"title":"User List","active":1,"parent":8,"path":"/user/list","icon":"","alias":"MasterUserList","sorting":1,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-01-30T21:31:49.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":256,"idMenu":9,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]},{"id":10,"title":"User Group","active":1,"parent":8,"path":"/usergroup","icon":"","alias":"MasterGroupList","sorting":2,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-01-30T21:35:24.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":257,"idMenu":10,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]}]},"9":{"id":9,"title":"User List","active":1,"parent":8,"path":"/user/list","icon":"","alias":"MasterUserList","sorting":1,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-01-30T21:31:49.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":256,"idMenu":9,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]},"10":{"id":10,"title":"User Group","active":1,"parent":8,"path":"/usergroup","icon":"","alias":"MasterGroupList","sorting":2,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-01-30T21:35:24.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":257,"idMenu":10,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]},"46":{"id":46,"title":"Menu Manager","active":1,"parent":7,"path":"/menu","icon":"fa fa-align-justify","alias":"MasterMenuManager","sorting":2,"className":"","badge":"","badgeClass":"","isExternalLink":"","isNavHeader":"","deleted":0,"createdAt":"0000-00-00 00:00:00","updatedAt":"2019-03-19T19:37:01.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":260,"idMenu":46,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]},"48":{"id":48,"title":"Config","active":1,"parent":7,"path":"/config","icon":"fa fa-cogs","alias":"config","sorting":3,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-02-07T05:31:58.000Z","updatedAt":"2019-03-19T19:37:17.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":261,"idMenu":48,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]},"49":{"id":49,"title":"Activity Logs","active":1,"parent":7,"path":"/activity-log","icon":"fa fa-bug","alias":"ActivityLogs","sorting":4,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-02-07T20:34:20.000Z","updatedAt":"2019-04-16T03:45:58.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":262,"idMenu":49,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]},"50":{"id":50,"title":"Target Operasi","active":1,"parent":7,"path":"/operasi/target","icon":"fa fa-cogs","alias":"MasterTargetList","sorting":7,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-04-24T21:38:55.000Z","updatedAt":"2019-04-24T22:05:48.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":263,"idMenu":50,"bitCreate":true,"bitEdit":true,"bitDelete":true,"bitView":true,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":true,"permission":1,"submenu":[],"bitPrint":false},"51":{"id":51,"title":"Jenis Operasi","active":1,"parent":7,"path":"/operasi/jenis","icon":"fa fa-cogs","alias":"MasterJenisList","sorting":7,"className":null,"badge":null,"badgeClass":null,"isExternalLink":null,"isNavHeader":null,"deleted":0,"createdAt":"2019-04-24T22:01:15.000Z","updatedAt":"2019-04-24T22:01:25.000Z","authAction":"[\\"bitCreate\\",\\"bitEdit\\",\\"bitDelete\\",\\"bitView\\",\\"bitPrint\\",\\"bitCancel\\",\\"bitApprove\\",\\"bitExcel\\"]","idAuth":264,"idMenu":51,"bitCreate":1,"bitEdit":1,"bitDelete":1,"bitView":1,"bitMenu":0,"bitCancel":0,"bitApprove":0,"bitExcel":1,"permission":1,"submenu":[]}},"remove":{},"add":{}}', 29, 'admin2', 0, '2019-04-25 05:06:12', '2019-04-25 05:06:12'),
(8, '', 'user/update', 'PUT', '{"id":"29","fullname":"Admin Backup","email":"admin@admin","username":"admin2","userGroup":"1","active":"1"}', 1, 'admin', 0, '2019-04-25 05:32:08', '2019-04-25 05:32:08');

-- --------------------------------------------------------

--
-- Table structure for table `md_katalog_target_operasi`
--

CREATE TABLE IF NOT EXISTS `md_katalog_target_operasi` (
  `id` int(11) unsigned NOT NULL,
  `target_operasi_id` int(11) DEFAULT NULL,
  `kelompok` varchar(50) DEFAULT NULL,
  `sub_kelompok` varchar(50) DEFAULT NULL,
  `uraian` varchar(100) DEFAULT NULL,
  `keterangan_operasi_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `md_keterangan_operasi`
--

CREATE TABLE IF NOT EXISTS `md_keterangan_operasi` (
  `id` int(11) unsigned NOT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `md_keterangan_operasi`
--

INSERT INTO `md_keterangan_operasi` (`id`, `keterangan`, `createdAt`, `updatedAt`) VALUES
(1, 'Perkara', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Unit', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Orang', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Buah', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Lokasi', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Rp', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Kali', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `md_polda`
--

CREATE TABLE IF NOT EXISTS `md_polda` (
  `id` int(11) unsigned NOT NULL,
  `nama_polda` varchar(30) DEFAULT NULL,
  `wilayah_hukum` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `md_polres`
--

CREATE TABLE IF NOT EXISTS `md_polres` (
  `id` int(11) unsigned NOT NULL,
  `polda_id` int(11) DEFAULT NULL,
  `nama_polres` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `md_rencana_operasi`
--

CREATE TABLE IF NOT EXISTS `md_rencana_operasi` (
  `id` int(11) unsigned NOT NULL,
  `tahun` char(4) DEFAULT NULL,
  `jenis_operasi_id` int(11) DEFAULT NULL,
  `no_operasi` varchar(30) DEFAULT NULL,
  `tgl_mulai` date DEFAULT NULL,
  `tgl_akhir` date DEFAULT NULL,
  `total_pelaksanaan` smallint(2) DEFAULT NULL,
  `catatan` varchar(255) DEFAULT NULL,
  `file_sprint` varchar(100) DEFAULT NULL,
  `status_renops` char(1) DEFAULT 'D' COMMENT 'D: Draft, P:Publish',
  `status_operasi` char(1) DEFAULT 'W' COMMENT 'W:Waiting, R: Running',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `md_target_operasi`
--

CREATE TABLE IF NOT EXISTS `md_target_operasi` (
  `id` int(11) unsigned NOT NULL,
  `jenis_data` varchar(100) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `md_target_operasi`
--

INSERT INTO `md_target_operasi` (`id`, `jenis_data`, `createdAt`, `updatedAt`, `deleted`) VALUES
(1, 'Data Terkait Masalah Pelanggaran Lalu Lintas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(2, 'Data Terkait Masalah Kecelakaan Lalu Lintas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(3, 'Data Terkait DIKMAS Lantas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(4, 'Data Terkait GIAT Kepolisians', '0000-00-00 00:00:00', '2019-04-25 04:53:43', 0);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `active` tinyint(4) DEFAULT '1',
  `parent` int(11) NOT NULL DEFAULT '0',
  `path` varchar(45) DEFAULT NULL,
  `icon` varchar(45) DEFAULT NULL,
  `alias` varchar(100) NOT NULL,
  `sorting` int(11) NOT NULL DEFAULT '0',
  `className` varchar(45) DEFAULT NULL,
  `badge` varchar(45) DEFAULT NULL,
  `badgeClass` varchar(45) DEFAULT NULL,
  `isExternalLink` varchar(45) DEFAULT NULL,
  `isNavHeader` varchar(45) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `authAction` varchar(200) DEFAULT '["bitCreate","bitEdit","bitDelete","bitView","bitPrint","bitCancel","bitApprove","bitExcel"]'
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `title`, `active`, `parent`, `path`, `icon`, `alias`, `sorting`, `className`, `badge`, `badgeClass`, `isExternalLink`, `isNavHeader`, `deleted`, `createdAt`, `updatedAt`, `authAction`) VALUES
(1, 'Dashboard', 1, 0, '/dashboard/index', 'ft-home', 'Home', 1, '', '', '', '', '', 0, '0000-00-00 00:00:00', '2019-02-07 07:42:56', '["bitCreate","bitEdit","bitDelete","bitView","bitPrint","bitCancel","bitApprove","bitExcel"]'),
(7, 'Master', 1, 0, '', 'ft-folder', 'Master', 2, '', '', '', '', '', 0, '0000-00-00 00:00:00', '2019-02-07 07:15:51', '["bitCreate","bitEdit","bitDelete","bitView","bitPrint","bitCancel","bitApprove","bitExcel"]'),
(8, 'User', 1, 7, '', 'ft-user', 'MasterUser', 1, '', '', '', '', '', 0, '0000-00-00 00:00:00', '2019-03-04 08:44:02', '["bitCreate","bitEdit","bitDelete","bitView","bitPrint","bitCancel","bitApprove","bitExcel"]'),
(9, 'User List', 1, 8, '/user/list', '', 'MasterUserList', 1, '', '', '', '', '', 0, '0000-00-00 00:00:00', '2019-01-31 04:31:49', '["bitCreate","bitEdit","bitDelete","bitView","bitPrint","bitCancel","bitApprove","bitExcel"]'),
(10, 'User Group', 1, 8, '/usergroup', '', 'MasterGroupList', 2, '', '', '', '', '', 0, '0000-00-00 00:00:00', '2019-01-31 04:35:24', '["bitCreate","bitEdit","bitDelete","bitView","bitPrint","bitCancel","bitApprove","bitExcel"]'),
(46, 'Menu Manager', 1, 7, '/menu', 'fa fa-align-justify', 'MasterMenuManager', 2, '', '', '', '', '', 0, '0000-00-00 00:00:00', '2019-03-20 02:37:01', '["bitCreate","bitEdit","bitDelete","bitView","bitPrint","bitCancel","bitApprove","bitExcel"]'),
(48, 'Config', 1, 7, '/config', 'fa fa-cogs', 'config', 3, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-07 12:31:58', '2019-03-20 02:37:17', '["bitCreate","bitEdit","bitDelete","bitView","bitPrint","bitCancel","bitApprove","bitExcel"]'),
(49, 'Activity Logs', 1, 7, '/activity-log', 'fa fa-bug', 'ActivityLogs', 4, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-08 03:34:20', '2019-04-16 10:45:58', '["bitCreate","bitEdit","bitDelete","bitView","bitPrint","bitCancel","bitApprove","bitExcel"]'),
(50, 'Target Operasi', 1, 7, '/operasi/target', 'fa fa-cogs', 'MasterTargetList', 7, NULL, NULL, NULL, NULL, NULL, 0, '2019-04-25 04:38:55', '2019-04-25 05:05:48', '["bitCreate","bitEdit","bitDelete","bitView","bitPrint","bitCancel","bitApprove","bitExcel"]'),
(51, 'Jenis Operasi', 1, 7, '/operasi/jenis', 'fa fa-cogs', 'MasterJenisList', 7, NULL, NULL, NULL, NULL, NULL, 0, '2019-04-25 05:01:15', '2019-04-25 05:01:25', '["bitCreate","bitEdit","bitDelete","bitView","bitPrint","bitCancel","bitApprove","bitExcel"]');

-- --------------------------------------------------------

--
-- Table structure for table `ref_jenis_operasi`
--

CREATE TABLE IF NOT EXISTS `ref_jenis_operasi` (
  `id` int(11) unsigned NOT NULL,
  `jenis_operasi` varchar(30) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_jenis_operasi`
--

INSERT INTO `ref_jenis_operasi` (`id`, `jenis_operasi`, `createdAt`, `updatedAt`, `deleted`) VALUES
(1, 'Operasi Zebra', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(2, 'Operasi Lilin', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(3, 'Operasi Patuh', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(4, 'Operasi Ketupat', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(5, 'Operasi Simpatik', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ref_jenis_user`
--

CREATE TABLE IF NOT EXISTS `ref_jenis_user` (
  `id` int(11) unsigned NOT NULL,
  `jenis_user` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_jenis_user`
--

INSERT INTO `ref_jenis_user` (`id`, `jenis_user`) VALUES
(1, 'MABES'),
(2, 'POLDA'),
(3, 'POLRES');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL COMMENT 'ID User',
  `fullname` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL COMMENT 'Nama User',
  `password` varchar(100) NOT NULL COMMENT 'Password',
  `email` varchar(45) NOT NULL,
  `otp` varchar(45) DEFAULT NULL,
  `otpTime` datetime NOT NULL,
  `idUserGroup` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  `lastlogin` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `createdBy` int(11) DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `companyId` int(11) NOT NULL DEFAULT '0',
  `shiftmentId` int(11) NOT NULL DEFAULT '0',
  `departmentId` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `fullname`, `username`, `password`, `email`, `otp`, `otpTime`, `idUserGroup`, `active`, `lastlogin`, `deleted`, `createdBy`, `updatedBy`, `createdAt`, `updatedAt`, `companyId`, `shiftmentId`, `departmentId`) VALUES
(1, 'Admin', 'admin', '$2a$10$AZXIDaRjSsZr6/Q2h0jKc.FqDo1A2cJnNW1.LAMew1u179YKQAvi.', '', NULL, '0000-00-00 00:00:00', 1, 1, NULL, 0, NULL, NULL, '2018-02-02 20:52:57', '2018-06-04 20:52:57', 0, 0, 0),
(29, 'Admin Backup', 'admin2', '$2a$10$AZXIDaRjSsZr6/Q2h0jKc.FqDo1A2cJnNW1.LAMew1u179YKQAvi.', 'admin@admin', NULL, '0000-00-00 00:00:00', 1, 1, NULL, 0, NULL, NULL, '0000-00-00 00:00:00', '2019-04-25 05:32:08', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `usertoken`
--

CREATE TABLE IF NOT EXISTS `usertoken` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `token` text NOT NULL,
  `refreshToken` text NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE IF NOT EXISTS `user_group` (
  `id` int(11) NOT NULL,
  `groupName` varchar(45) DEFAULT NULL,
  `apiCrud` varchar(45) DEFAULT '["POST","PUT","GET","DELETE"]',
  `apiActionDisabled` text,
  `createdBy` int(11) DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`id`, `groupName`, `apiCrud`, `apiActionDisabled`, `createdBy`, `updatedBy`, `deleted`, `createdAt`, `updatedAt`) VALUES
(1, 'Administrator', '["POST","PUT","GET","DELETE"]', '{}', NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'POLDA', '["POST","PUT","GET","DELETE"]', '{}', NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'POLRES', '["POST","PUT","GET","DELETE"]', '{}', NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'MABES', '["POST","PUT","GET","DELETE"]', '{}', NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authorization`
--
ALTER TABLE `authorization`
  ADD PRIMARY KEY (`id`),
  ADD KEY `groupAndModule` (`idMenu`,`idObject`),
  ADD KEY `idxObject` (`idObject`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `md_katalog_target_operasi`
--
ALTER TABLE `md_katalog_target_operasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `md_keterangan_operasi`
--
ALTER TABLE `md_keterangan_operasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `md_polda`
--
ALTER TABLE `md_polda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `md_polres`
--
ALTER TABLE `md_polres`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `md_rencana_operasi`
--
ALTER TABLE `md_rencana_operasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `md_target_operasi`
--
ALTER TABLE `md_target_operasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `active` (`active`);

--
-- Indexes for table `ref_jenis_operasi`
--
ALTER TABLE `ref_jenis_operasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ref_jenis_user`
--
ALTER TABLE `ref_jenis_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `VCH_USERNAME_UNIQUE` (`username`);

--
-- Indexes for table `usertoken`
--
ALTER TABLE `usertoken`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `authorization`
--
ALTER TABLE `authorization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=270;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `md_katalog_target_operasi`
--
ALTER TABLE `md_katalog_target_operasi`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `md_keterangan_operasi`
--
ALTER TABLE `md_keterangan_operasi`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `md_polda`
--
ALTER TABLE `md_polda`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `md_polres`
--
ALTER TABLE `md_polres`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `md_rencana_operasi`
--
ALTER TABLE `md_rencana_operasi`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `md_target_operasi`
--
ALTER TABLE `md_target_operasi`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `ref_jenis_operasi`
--
ALTER TABLE `ref_jenis_operasi`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ref_jenis_user`
--
ALTER TABLE `ref_jenis_user`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID User',AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `usertoken`
--
ALTER TABLE `usertoken`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_group`
--
ALTER TABLE `user_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
